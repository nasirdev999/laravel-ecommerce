<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ContactController extends Controller
{
    public function seenList()
    {
    	$data['seenlists'] = Contact::where('status','1')->get();
    	return view('backend.admin.contact.seenlist',$data);
    }

    public function uneenList()
    {
    	$data['unseenlists'] = Contact::where('status','0')->get();
    	return view('backend.admin.contact.unseenlist',$data);
    }

    public function uneenListView($id)
    {
    	$data['messageView'] = Contact::where('id',$id)->where('status','0')->first();
    	return view('backend.admin.contact.unseenView',$data);
    }

    public function uneenUpdate($id)
    {
    	$contactUpdate = Contact::find($id);
    	$contactUpdate->status = '1';
    	$contactUpdate->save();

    	Toastr::success('Message Seen Successfull', 'Success');
    	return redirect()->route('admin.contact.seen.list');
    }

    public function messageDelete($id)
    {
    	$contactDel = Contact::find($id)->delete();
    	
    	Toastr::success('Message Deleted Successfull', 'Success');
    	return redirect()->back();
    }

    public function messageReply($id)
    {
    	$data['msgReply'] = Contact::where('id',$id)->first();
    	return view('backend.admin.contact.replymessage',$data);
    }

    public function contactReply(Request $request)
    {
        $this->validate($request,[
            'toemail' => 'required',
            'subject' => 'required',
            'messages' => 'required',
        ]);

        $data = array(
            'toemail' => $request->toemail,
            'fromemail' => $request->fromemail,
            'subject' => $request->subject,
            'messages' => $request->messages,
         );

         Mail::send('backend.admin.contact.sendView', $data, function($message) use($data){
            $message->from('likeproject12345@gmail.com','Smart Bazer');
            $message->to($data['toemail']);
            $message->subject('Smart Bazer !! Your Reply Message.');
         });

        Toastr::success('Message Sent Successfull','Success');
        return redirect()->route('admin.contact.seen.list');

    }
}

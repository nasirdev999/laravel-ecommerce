<?php

namespace App\Http\Controllers\Admin;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class AboutController extends Controller
{
    public function aboutIndex()
    {
    	$alldata = About::all();	
    	return view('backend.admin.about.aboutindex',compact('alldata'));
    }

    public function edit($id)
    {
    	$editData = About::find($id);	
    	return view('backend.admin.about.aboutupdate',compact('editData'));
    }

    public function aboutUpdate(Request $request, $id)
    {
    	$this->validate($request,[
    		'image1' => 'mimes:jpeg,jpg,png',
    		'image2' => 'mimes:jpeg,jpg,png',
    		'video' => 'required',
    		'details' => 'required',
    	]);

    	$about = About::find($id);
    	
    	$image1 = $request->file('image1');
    	if($image1){
    		$imagename1 = uniqid().'.'.$image1->getClientOriginalExtension();
    		$image1->move('upload/about/',$imagename1);
    		if(file_exists('upload/about/'.$about->image1)){
    			unlink('upload/about/'.$about->image1);
    		}
    		$about->image1 = $imagename1;
    	}

    	$image2 = $request->file('image2');
    	if($image2){
    		$imagename2 = uniqid().'.'.$image2->getClientOriginalExtension();
    		$image2->move('upload/about/',$imagename2);
    		if(file_exists('upload/about/'.$about->image2)){
    			unlink('upload/about/'.$about->image2);
    		}
    		$about->image2 = $imagename2;
    	}
    	
    	$about->video = $request->video;
    	$about->details = $request->details;
    	$about->update();

    	Toastr::success('About Information Updated Successfull');
    	return redirect()->route('admin.about.index');
    }

}

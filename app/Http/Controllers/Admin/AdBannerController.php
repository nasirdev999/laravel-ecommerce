<?php

namespace App\Http\Controllers\Admin;

use App\AdBanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = AdBanner::all();
        return view('backend.admin.adbanner.bannerIndex',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'heading1' => 'required',
            'heading2' => 'required',
            'pra1' => 'required',
            'pra2' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png',
        ]);

        $banner = new AdBanner();

        $banner->heading1 = $request->heading1;
        $banner->heading2 = $request->heading2;
        $banner->pra1 = $request->pra1;
        $banner->pra2 = $request->pra2;

        $image = $request->file('image');
        if($image){
            $imagename = uniqid().'.'.$image->getClientOriginalExtension();
            $image->move('upload/banner/',$imagename);
            $banner->image = $imagename;
        }
        $banner->save();

        return redirect()->route('admin.banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = AdBanner::where('id',$id)->first();
        return view('backend.admin.adbanner.bannerEdit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'heading1' => 'required',
            'heading2' => 'required',
            'pra1' => 'required',
            'pra2' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
        ]);

        $banner = AdBanner::find($id);

        $banner->heading1 = $request->heading1;
        $banner->heading2 = $request->heading2;
        $banner->pra1 = $request->pra1;
        $banner->pra2 = $request->pra2;

        $image = $request->file('image');
        if($image){
            $imagename = uniqid().'.'.$image->getClientOriginalExtension();
            $image->move('upload/banner/',$imagename);
            unlink('upload/banner/'.$banner->image);
            $banner->image = $imagename;
        }

        $banner->update();

        return redirect()->route('admin.banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = Category::latest()->get();
        return view('backend.admin.category.categoryIndex',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:categories,name'
        ]);

        $category = new Category();

        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->created_by = Auth::id();
        $category->save();

        Toastr::success('Category Created Successfull ','Success');
        return redirect()->route('admin.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Category::where('id', $id)->first();
        return view('backend.admin.category.categoryEdit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:categories,name'
        ]);

        $category = Category::find($id);

        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->updated_by = Auth::id();
        $category->update();

        Toastr::success('Category Updated Successfull ','Success');
        return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id)->delete();
        Toastr::success('Category Deleted Successfull ','Success');
        return redirect()->route('admin.category.index');
    }
}

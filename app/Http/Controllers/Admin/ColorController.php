<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = Color::latest()->get();
        return view('backend.admin.color.colorIndex',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:colors,name'
        ]);

        $color = new Color();
        $color->name = $request->name;
        $color->slug = str_slug($request->name);
        $color->created_by = Auth::id();
        $color->save();

        Toastr::success('Color Created Successfull ','Success');
        return redirect()->route('admin.color.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Color::where('id', $id)->first();
        return view('backend.admin.color.colorEdit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:colors,name'
        ]);

        $color = Color::find($id);
        $color->name = $request->name;
        $color->slug = str_slug($request->name);
        $color->updated_by = Auth::id();
        $color->update();

        Toastr::success('Color Updated Successfull ','Success');
        return redirect()->route('admin.color.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

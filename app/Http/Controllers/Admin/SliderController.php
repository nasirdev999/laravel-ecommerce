<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = Slider::latest()->get();
        return view('backend.admin.slider.sliderIndex',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'heading1' => 'required',
            'heading2' => 'required',
            'para1' => 'required',
            'para2' => 'required',
            'discount' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $slider = new Slider();

        $slider->heading1 = $request->heading1;
        $slider->heading2 = $request->heading2;
        $slider->para1 = $request->para1;
        $slider->para2 = $request->para2;
        $slider->discount = $request->discount;

        $image = $request->file('image');
        if($image){
            $imagename = str_slug($request->heading1).'-'.uniqid().'.'.$image->getClientOriginalExtension();
            $image->move('upload/sliderImage/',$imagename);
        }
        $slider->image = $imagename;
        $slider->save();

        Toastr::success('Slider Created Successfull','Success');
        return redirect()->route('admin.slider.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Slider::where('id', $id)->first();
        return view('backend.admin.slider.sliderEdit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'heading1' => 'required',
            'heading2' => 'required',
            'para1' => 'required',
            'para2' => 'required',
            'discount' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
        ]);

        $slider = Slider::find($id);

        $slider->heading1 = $request->heading1;
        $slider->heading2 = $request->heading2;
        $slider->para1 = $request->para1;
        $slider->para2 = $request->para2;
        $slider->discount = $request->discount;

        $image = $request->file('image');
        if($image){
            $imagename = str_slug($request->heading1).'-'.uniqid().'.'.$image->getClientOriginalExtension();
            $image->move('upload/sliderImage/',$imagename);
            
            if(file_exists('upload/sliderImage/'.$slider->image)){
                unlink('upload/sliderImage/'.$slider->image);
            }

            $slider->image = $imagename;
        }

       
        $slider->update();

        Toastr::success('Slider Updated Successfull','Success');
        return redirect()->route('admin.slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);

        if(file_exists('upload/sliderImage/'.$slider->image)){
            unlink('upload/sliderImage/'.$slider->image);
        }
        $slider->delete();

        Toastr::success('Slider Deleted Successfull','Success');
        return redirect()->route('admin.slider.index');
    }
}

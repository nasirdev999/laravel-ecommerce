<?php

namespace App\Http\Controllers\Admin;

use App\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class SubsribeController extends Controller
{
    public function index()
    {
    	$alldata = Subscribe::latest()->get();
    	return view('backend.admin.subscribe.subscribeIndex',compact('alldata'));
    }
}

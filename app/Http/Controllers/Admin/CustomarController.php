<?php

namespace App\Http\Controllers\Admin;

use App\Customar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class CustomarController extends Controller
{
    public function index()
    {
    	$data['alldata'] = Customar::where('status','1')->get();
    	return view('backend.admin.customar.cusindex',$data);
    }

    public function unverifyList()
    {
    	$data['alldata'] = Customar::where('status','0')->get();
    	return view('backend.admin.customar.cusUnverifylist',$data);
    }

    public function customarDestroy($id)
    {
    	$customar = Customar::find($id)->delete();
    	Toastr::success('Customar Deleted Successfull','Success');
    	return redirect()->route('admin.customar.unverify.list');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class BrandController extends Controller
{
    public function index()
    {
    	return view('backend.admin.brand.brandIndex');
    }

    public function allData()
    {
    	$alldata = Brand::latest()->get();
    	return response()->json($alldata);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|unique:brands,name'
    	]);

    	$brand = new Brand();
    	$brand->name = $request->name;
    	$brand->slug = str_slug($request->name);
    	$brand->created_by = Auth::id();
    	$brand->save();
    	return response()->json($brand);
    }

    public function edit($id)
    {
    	$brand = Brand::where('id', $id)->first();
    	return response()->json($brand);
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request, [
    		'name' => 'required|unique:brands,name'
    	]);

    	$brand = Brand::findOrFail($id);
    	$brand->name = $request->name;
    	$brand->slug = str_slug($request->name);
    	$brand->updated_by = Auth::id();
    	$brand->update();
    	return response()->json($brand);
    }

    public function destroy($id)
    {
    	$brand = Brand::findOrFail($id)->delete();
    	return response()->json($brand);
    }


}

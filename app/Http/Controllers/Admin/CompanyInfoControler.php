<?php

namespace App\Http\Controllers\Admin;

use App\CompanyInfo;
use App\CompanyLogo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class CompanyInfoControler extends Controller
{
    public function index()
    {
    	$alldata = CompanyInfo::all();
    	return view('backend.admin.companyInfo.companyInfo',compact('alldata'));
    }

    public function edit($id)
    {
    	$editData = CompanyInfo::find($id);
    	return view('backend.admin.companyInfo.edit',compact('editData'));
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request,[
    		'email' => 'required',
    		'phone' => 'required',
    		'fax' => 'required',
    		'address' => 'required',
    		'facebook' => 'required',
    		'twitter' => 'required',
    		'google' => 'required',
    	]);

    	$companyUpdate = CompanyInfo::find($id);

    	$companyUpdate->email = $request->email;
    	$companyUpdate->phone = $request->phone;
    	$companyUpdate->fax = $request->fax;
    	$companyUpdate->address = $request->address;
    	$companyUpdate->facebook = $request->facebook;
    	$companyUpdate->twitter = $request->twitter;
    	$companyUpdate->gooleplus = $request->google;
    	$companyUpdate->save();

    	Toastr::success('Information Updated Successfull','Success');
    	return redirect()->route('admin.company.info.index');
    }

    public function logoIndex()
    {
        $alldata = CompanyLogo::all();
        return view('backend.admin.companyInfo.companyLogo',compact('alldata'));
    }

    public function logoEdit($id)
    {
        $editData = CompanyLogo::find($id);
        return view('backend.admin.companyInfo.logoedit',compact('editData'));
    }

    public function logoUpdate(Request $request, $id)
    {
        $this->validate($request,[
            'special_later1' => 'required',
            'special_later2' => 'required',
            'word1' => 'required',
            'word2' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
        ]);

        $companyLogo = CompanyLogo::find($id);

        $companyLogo->special_later1 = $request->special_later1;
        $companyLogo->special_later2 = $request->special_later2;
        $companyLogo->word1 = $request->word1;
        $companyLogo->word2 = $request->word2;

        $image = $request->file('image');
        if($image){
            $imagename = uniqid().'.'.$image->getClientOriginalExtension();
            $image->move('upload/companyLogo/',$imagename);
            unlink('upload/companyLogo/'.$companyLogo->image);
        }
         $companyLogo->image = $imagename;

        $companyLogo->save();

        Toastr::success('Company Logo Updated Successfull','Success');
        return redirect()->route('admin.company.logo.index');
    }

}

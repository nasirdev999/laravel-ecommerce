<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Brand;
use App\Order;
use App\Contact;
use App\Product;
use App\Category;
use App\Customar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller
{
    public function index()
    {
    	$data['user_count'] = User::all();
    	$data['order_count'] = Order::all();
    	$data['brand_count'] = Brand::all();
    	$data['contact_count'] = Contact::all();
    	$data['product_count'] = Product::all();
    	$data['category_count'] = Category::all();
    	$data['customar_count'] = Customar::all();
    	$data['some_orders'] = Order::latest()->take(7)->get();
    	$data['some_products'] = Product::latest()->take(5)->get();
    	return view('backend.admin.dashboard',$data);
    }
}

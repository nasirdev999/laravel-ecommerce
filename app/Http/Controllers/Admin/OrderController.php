<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use App\Unit;
use App\Brand;
use App\Color;
use App\Product;
use App\Category;
use App\Checkout;
use App\ProductUnit;
use App\MadeCountry;
use App\Order;
use App\Payment;
use App\OrderDetail;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;


class OrderController extends Controller
{
    public function orderPendingList()
    {
    	$alldata = Order::where('status','0')->get();
    	return view('backend.admin.order.orderPending',compact('alldata'));
    }

    public function orderApproveList()
    {
    	$alldata = Order::where('status','1')->get();
    	return view('backend.admin.order.orderApprove',compact('alldata'));
    }

    public function orderApproval($id)
    {
    	$data['invoiceOrder'] = Order::where('id', $id)->first();
    	return view('backend.admin.order.approveInvoice',$data);
    }

    public function orderApproveUpdate(Request $request, $id)
    {
    	$orderApprove = Order::find($id);
    	$orderApprove->status = '1';
    	$orderApprove->update();
    	return redirect()->route('admin.order.approve');
    }


}

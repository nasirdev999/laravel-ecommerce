<?php

namespace App\Http\Controllers\Admin;

use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class UserController extends Controller
{
    public function index()
    {
    	$data['alldata'] = User::latest()->get();
    	return view('backend.admin.user.userIndex',$data);
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
    		'role_id' => 'required',
    		'password' => 'required|min:6',
    	]);

    	$users = new User();

    	$users->name = $request->name;
    	$users->email = $request->email;
    	$users->role_id = $request->role_id;

        if($request->role_id == 1){
            $users->userType = 'admin';
        }elseif($request->role_id == 2){
            $users->userType = 'author';
        }elseif($request->role_id == 3){
            $users->userType = 'editor';
        }

    	$users->password = Hash::make($request->password);
    	$users->save();

    	Toastr::success('User Created Successfull ','Success');
    	return redirect()->route('admin.user.index');
    }

    public function edit($id)
    {
    	$data['editData'] = User::where('id',$id)->first();
    	return view('backend.admin.user.userEdit',$data);
    }

    public function update(Request $request, $id)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
    		'role_id' => 'required',
    		'password' => 'required|min:6',
    	]);

    	$users = User::find($id);

    	$users->name = $request->name;
    	$users->email = $request->email;
    	$users->role_id = $request->role_id;
    	$users->password = Hash::make($request->password);
    	$users->update();

    	Toastr::success('User Updated Successfull ','Success');
    	return redirect()->route('admin.user.index');
    }


}

<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\MadeCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class MadeCountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = MadeCountry::latest()->get();
        return view('backend.admin.madeCountry.madeIndex',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:made_countries,name'
        ]);

        $country = new MadeCountry();

        $country->name = $request->name;
        $country->slug = str_slug($request->name);
        $country->created_by = Auth::id();
        $country->save();

        Toastr::success('Country Created Successfull ','Success');
        return redirect()->route('admin.made-country.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = MadeCountry::where('id', $id)->first();
        return view('backend.admin.madeCountry.madeEdit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:made_countries,name'
        ]);

        $country = MadeCountry::find($id);

        $country->name = $request->name;
        $country->slug = str_slug($request->name);
        $country->updated_by = Auth::id();
        $country->update();

        Toastr::success('Country Updated Successfull ','Success');
        return redirect()->route('admin.made-country.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Author;

use Hash;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class SettingController extends Controller
{
    public function userProfile()
    {
    	$userData = User::find(Auth::id());
    	return view('backend.author.setting.profile',compact('userData'));
    }

    public function userUpdate(Request $request, $id)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required',
    		'address' => 'required',
    		'image' => 'mimes:jpe,jpeg,png',
    		'details' => 'required',
    	]);

    	$userUpdate = User::find(Auth::id());

    	$userUpdate->name = $request->name;
    	$userUpdate->email = $request->email;
    	$userUpdate->address = $request->address;
    	$userUpdate->details = $request->details;

    	$image = $request->file('image');
    	if($image){
    		$imagename = str_slug($request->name).'-'.uniqid().'.'.$image->getClientOriginalExtension();
    		$image->move('upload/userImage/',$imagename);

    		if(file_exists('upload/userImage/'.$userUpdate->image)){
    			unlink('upload/userImage/'.$userUpdate->image);
    		}
    		$userUpdate->image = $imagename;
    	}

    	$userUpdate->save();

    	Toastr::success('User Profile Successfull ','Success');
    	return redirect()->back();
    }

    public function userPassword()
    {
    	return view('backend.author.setting.passwordChange');
    }

    public function userPasswordUpdate(Request $request)
    {
    	$this->validate($request, [
    		'old_password' => 'required',
    		'password' => 'required|confirmed'
    	]);

    	$hasPassword = Auth::user()->password;

    	if(Hash::check($request->old_password, $hasPassword))
    	{
    		if(!Hash::check($request->password, $hasPassword))
    		{
    			$user = User::find(Auth::id());
    			$user->password = Hash::make($request->password);
    			$user->save();
    			Toastr::success('Password Successfull Change','Success');
    			Auth::logout();
    			return redirect()->back();
                
    		}else{
    			Toastr::error('New Password Cannot be Old Password','Error');
    			return redirect()->back();
    		}
    	}else{
    		Toastr::error('Old Password Not Match','Error');
    		return redirect()->back();
    	}

    }
}

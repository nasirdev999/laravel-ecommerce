<?php

namespace App\Http\Controllers\Author;

use DB;
use Auth;
use App\User;
use App\Unit;
use App\Brand;
use App\Color;
use App\Product;
use App\Category;
use App\ProductUnit;
use App\MadeCountry;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;


class ProductController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $data['alldata'] = Auth::User(Auth::id())->products()->latest()->get();
       $data['alldata'] = Product::where('created_by',Auth::id())->get();
        return view('backend.author.product.productIndex',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['colors'] = Color::all();
        $data['units'] = Unit::all();
        $data['categoris'] = Category::all();
        $data['brands'] = Brand::all();
        $data['countris'] = MadeCountry::all();
        return view('backend.author.product.productCreate',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'title' => 'required|unique:products,title',
            'category_id' => 'required',
            'subCategory_id' => 'required',
            'brand_id' => 'required',
            'MadeCountry_id' => 'required',
            'buying_price' => 'required',
            'selling_price' => 'required',
            'stock' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'short_details' => 'required',
            'long_details' => 'required',
        ]);

        DB::transaction(function() use($request){
            
            $product = new Product();

            $images = $request->file('image');
            if($images){
                $imagename = str_slug($request->name).'-'.uniqid().'.'.$images->getClientOriginalExtension();
                $images->move('upload/productImage/',$imagename);
                $product->image = $imagename;
            }

            
            $product->name = $request->name;
            $product->title = $request->title;
            $product->slug = str_slug($request->title);
            $product->category_id = $request->category_id;
            $product->subCategory_id = $request->subCategory_id;
            $product->brand_id = $request->brand_id;
            $product->MadeCountry_id = $request->MadeCountry_id;
            $product->buying_price = $request->buying_price;
            $product->selling_price = $request->selling_price;
            $product->discount_percent = $request->discount_percent;
            $pro_discount_price = ($product->selling_price / 100)*$product->discount_percent;
            $product->discount_price = ($product->selling_price) - $pro_discount_price;
            $product->stock = $request->stock;

            if(isset($request->status)){
                $product->status = true;
            }else{
                $product->status = false;
            }

            $product->is_approve = false;
            
            $product->created_by = Auth::id();
            $product->short_details = $request->short_details;
            $product->long_details = $request->long_details;

            if($product->save())
            {
                $units = $request->units_id;
                if($units){
                    foreach($units as $unit){
                        $pro_unit = new ProductUnit();
                        $pro_unit->product_id = $product->id;
                        $pro_unit->units_id = $unit;
                        $pro_unit->save();
                    }
                }

                $colors = $request->color_id;
                if($colors){
                    foreach($colors as $color){
                        $pro_color = new ProductColor();
                        $pro_color->product_id = $product->id;
                        $pro_color->color_id = $color;
                        $pro_color->save();
                    }
                }

                $files = $request->sub_image;
                if($files)
                {
                    foreach($files as $file)
                    {
                        $subImagename = uniqid().'.'.$file->getClientOriginalExtension();
                        $file->move('upload/productImage/productSubImage/',$subImagename);
                        //$productSubImage['sub_image']  = $subImagename;

                        $productSubImage = new ProductSubImage();
                        $productSubImage->product_id = $product->id;
                        $productSubImage->sub_image  = $subImagename;
                        $productSubImage->save();
                    }
                }


            }

        });

        Toastr::success('Product Created Successfull', 'Success');
        return redirect()->route('author.product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['showData'] = Product::where('id', $id)->first(); 

        if(Auth::id() != $data['showData']->created_by){
            Toastr::error("This authriaze is not access !!", 'Error');
            return redirect()->back();
        }

        $data['colors'] = ProductColor::where('product_id',$id)->get();
        $data['units'] = ProductUnit::where('product_id',$id)->get();
        $data['sub_images'] = ProductSubImage::where('product_id',$id)->get();
        return view('backend.author.product.productShow',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['colors'] = Color::all();
        $data['units'] = Unit::all();
        $data['categoris'] = Category::all();
        $data['brands'] = Brand::all();
        $data['countris'] = MadeCountry::all();
        $data['editData'] = Product::where('id', $id)->first();
        $data['subCategory'] = SubCategory::latest()->get();
        $data['sub_images'] = ProductSubImage::where('product_id', $id)->get();
        $data['productUnits'] = ProductUnit::where('product_id', $id)->get();
        $data['productColor'] = ProductColor::where('product_id', $id)->get();

        if(Auth::id() != $data['editData']->created_by){
            Toastr::error("This authriaze is not access !!", 'Error');
            return redirect()->back();
        }

        return view('backend.author.product.productEdit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'title' => 'required|unique:products,title',
            'category_id' => 'required',
            'subCategory_id' => 'required',
            'brand_id' => 'required',
            'MadeCountry_id' => 'required',
            'buying_price' => 'required',
            'selling_price' => 'required',
            'stock' => 'required',
            'image' => 'mimes:jpg,jpeg,png',
            'short_details' => 'required',
            'long_details' => 'required',
        ]);

        DB::transaction(function() use($request, $id){
            
            $product = Product::find($id);

            $images = $request->file('image');
            if($images){
                $imagename = str_slug($request->name).'-'.uniqid().'.'.$images->getClientOriginalExtension();
                $images->move('upload/productImage/',$imagename);
                if(file_exists('upload/productImage/'.$product->image)){
                    unlink('upload/productImage/'.$product->image);
                }
               
                $product->image = $imagename;
            }

            
            $product->name = $request->name;
            $product->title = $request->title;
            $product->slug = str_slug($request->title);
            $product->category_id = $request->category_id;
            $product->subCategory_id = $request->subCategory_id;
            $product->brand_id = $request->brand_id;
            $product->MadeCountry_id = $request->MadeCountry_id;
            $product->buying_price = $request->buying_price;
            $product->selling_price = $request->selling_price;
            $product->discount_percent = $request->discount_percent;
            $pro_discount_price = ($product->selling_price / 100)*$product->discount_percent;
            $product->discount_price = ($product->selling_price) - $pro_discount_price;
            $product->stock = $request->stock;

            if(isset($request->status)){
                $product->status = true;
            }else{
                $product->status = false;
            }

            $product->is_approve = false;
          
            
            $product->updated_by = Auth::id();
            $product->short_details = $request->short_details;
            $product->long_details = $request->long_details;

            if($product->update())
            {
                $units = $request->units_id;

                if(!empty($units)){
                    ProductUnit::where('product_id', $id)->delete();
                }

                if($units){
                    foreach($units as $unit){
                        $pro_unit = new ProductUnit();
                        $pro_unit->product_id = $product->id;
                        $pro_unit->units_id = $unit;
                        $pro_unit->save();
                    }
                }

                $colors = $request->color_id;

                if(!empty($colors)){
                    ProductColor::where('product_id', $id)->delete();
                }

                if($colors){
                    foreach($colors as $color){
                        $pro_color = new ProductColor();
                        $pro_color->product_id = $product->id;
                        $pro_color->color_id = $color;
                        $pro_color->save();
                    }
                }

                $files = $request->sub_image;

                if(!empty($files)){
                    $subimageDelete =  ProductSubImage::where('product_id', $id)->get();
                    foreach($subimageDelete as $imageValue){
                        if (!empty($imageValue)) {
                            unlink('upload/productImage/productSubImage/'.$imageValue->sub_image);
                        }
                    }
                   ProductSubImage::where('product_id', $id)->delete();
                }

                if($files)
                {
                    foreach($files as $file)
                    {
                        $subImagename = uniqid().'.'.$file->getClientOriginalExtension();
                        $file->move('upload/productImage/productSubImage/',$subImagename);

                        $productSubImage = new ProductSubImage();
                        $productSubImage->product_id = $product->id;
                        $productSubImage->sub_image  = $subImagename;
                        $productSubImage->save();
                    }
                }


            }

        });

        Toastr::success('Product Updated Successfull', 'Success');
        return redirect()->route('author.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if(Auth::id() != $product->created_by){
            Toastr::error("This authriaze is not access !!", 'Error');
            return redirect()->back();
        }

        if(file_exists('upload/productImage/'.$product->image)){
            unlink('upload/productImage/'.$product->image);
        }
        $product->delete();

        ProductUnit::where('product_id',$id)->delete();
        ProductColor::where('product_id',$id)->delete();

        $sub_image = ProductSubImage::where('product_id',$id)->get();
        foreach($sub_image as $image){
            if(file_exists('upload/productImage/productSubImage/'.$image->sub_image))
            {
                unlink('upload/productImage/productSubImage/'.$image->sub_image);
            }
        }
        ProductSubImage::where('product_id', $id)->delete();

        Toastr::success('Product Deleted Successfull','Success');
        return redirect()->route('author.product.index');
    }

}

<?php

namespace App\Http\Controllers\Author;

use DB;
use Auth;
use App\Unit;
use App\Brand;
use App\Color;
use App\Product;
use App\Category;
use App\ProductUnit;
use App\MadeCountry;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class DefaultController extends Controller
{
    public function getSubCateogory(Request $request)
    {
    	$subCategory = SubCategory::where('category_id', $request->category_id)->get();
    	return response()->json($subCategory);
    }
}

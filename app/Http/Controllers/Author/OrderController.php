<?php

namespace App\Http\Controllers\Author;

use DB;
use Auth;
use App\Product;
use App\Checkout;
use App\Order;
use App\Payment;
use App\OrderDetail;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class OrderController extends Controller
{
    public function orderPendingList()
    {
    	$alldata = Order::where('status','0')->get();
    	return view('backend.author.order.orderPending',compact('alldata'));
    }

    public function orderApproveList()
    {
    	$alldata = Order::where('status','1')->get();
    	return view('backend.author.order.orderApprove',compact('alldata'));
    }

    public function orderApproval($id)
    {
    	$data['invoiceOrder'] = Order::where('id', $id)->first();
    	return view('backend.author.order.approveInvoice',$data);
    }

    public function orderApproveUpdate(Request $request, $id)
    {
    	$orderApprove = Order::find($id);
    	$orderApprove->status = '1';
    	$orderApprove->update();
    	return redirect()->route('author.order.approve');
    }
}

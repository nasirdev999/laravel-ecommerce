<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use App\Brand;
use App\Slider;
use App\Product;
use App\Category;
use App\AdBanner;
use App\CompanyLogo;
use App\CompanyInfo;
use App\MadeCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class HomePageController extends Controller
{
    public function homePageData()
    {
    	$data['brands'] = Brand::latest()->get();
        $data['sliders'] = Slider::all();
        $data['adBanners'] = AdBanner::where('id',1)->first();
    	$data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
    	$data['specialProduct'] = Product::take(4)->get();
    	$data['categoris'] = Category::latest()->get();
    	$data['madeCountry'] = MadeCountry::latest()->get();
    	$data['products'] = Product::take(15)->inRandomOrder()->Status()->Aproved()->get();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
    	return view('frontend.home.homepage',$data);
    }
}

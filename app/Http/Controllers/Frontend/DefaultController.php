<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Brand;
use App\Category;
use App\CompanyInfo;
use App\CompanyLogo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefaultController extends Controller
{
    public function aboutPages()
    {
        $data['brands'] = Brand::latest()->get();
        $data['abouts'] = About::where('id',1)->first();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        return view('frontend.about.aboutus',$data);
    }

    public function faqsPages()
    {
        $data['brands'] = Brand::latest()->get();
        //$data['abouts'] = About::where('id',1)->first();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        return view('frontend.faqs.faqs',$data);
    }

    public function helpPages()
    {
        $data['brands'] = Brand::latest()->get();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        return view('frontend.help.help',$data);
    }

    public function termsPages()
    {
        $data['brands'] = Brand::latest()->get();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        return view('frontend.terms.termsofuse',$data);
    }

    public function privacyPages()
    {
        $data['brands'] = Brand::latest()->get();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        return view('frontend.privacy.privacy',$data);
    }



}

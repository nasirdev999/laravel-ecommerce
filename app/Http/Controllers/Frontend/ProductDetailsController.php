<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use App\Brand;
use App\Product;
use App\Category;
use App\CompanyLogo;
use App\CompanyInfo;
use App\ProductUnit;
use App\MadeCountry;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ProductDetailsController extends Controller
{
    public function productDetails($slug)
    {
        $data['brands'] = Brand::latest()->get();
        $data['categoris'] = Category::latest()->get();
    	$data['productDetail'] = Product::where('slug', $slug)->first();
    	$data['pro_subImage'] = ProductSubImage::where('product_id', $data['productDetail']->id)->get();
    	$data['add_more_products'] = Product::where('category_id', $data['productDetail']->category_id)->Status()->Aproved()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
    	return view('frontend.pages.productDetails',$data);
    }

    public function categoryByPro($slug)
    {
    	$data['brands'] = Brand::latest()->get();
    	$data['specialProduct'] = Product::take(4)->get();
    	$data['categoris'] = Category::latest()->get();
    	$data['category_wise_data'] = Category::where('slug',$slug)->first();
    	$data['madeCountry'] = MadeCountry::latest()->get();
    	$data['cateProduct'] = Product::where('category_id',$data['category_wise_data']->id)->Status()->Aproved()->paginate(12);
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
    	return view('frontend.pages.categorybyProduct',$data);
    }

    public function brandByPro($slug)
    {
    	$data['brands'] = Brand::latest()->get();
    	$data['specialProduct'] = Product::take(4)->get();
    	$data['categoris'] = Category::latest()->get();
    	$data['brand_wise_data'] = Brand::where('slug',$slug)->first();
    	$data['madeCountry'] = MadeCountry::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
    	$data['brandProduct'] = Product::where('brand_id',$data['brand_wise_data']->id)->Status()->Aproved()->paginate(12);
    	return view('frontend.pages.brandbyProduct',$data);
    }

    public function countryByPro($slug)
    {
    	$data['brands'] = Brand::latest()->get();
    	$data['specialProduct'] = Product::take(4)->get();
    	$data['categoris'] = Category::latest()->get();
    	$data['madeCountry'] = MadeCountry::latest()->get();
    	$data['country_wise_data'] = MadeCountry::where('slug',$slug)->first();
    	$data['countryProduct'] = Product::where('MadeCountry_id',$data['country_wise_data']->id)->Status()->Aproved()->paginate(12);
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
    	return view('frontend.pages.countrybyProduct',$data);
    }

    public function allproduct()
    {
        $data['brands'] = Brand::latest()->get();
        $data['specialProduct'] = Product::take(4)->get();
        $data['categoris'] = Category::latest()->get();
        $data['madeCountry'] = MadeCountry::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
        $data['special_offers'] = Product::where('discount_percent','>',20)->Status()->Aproved()->get();
        $data['allproducts'] = Product::latest()->Status()->Aproved()->paginate(12);
        return view('frontend.pages.allproducts',$data);
    }

}

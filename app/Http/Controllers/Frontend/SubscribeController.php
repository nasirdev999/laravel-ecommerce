<?php

namespace App\Http\Controllers\Frontend;

use App\Subscribe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class SubscribeController extends Controller
{
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required|unique:subscribes,email',
    	]);

    	$subscribe = new Subscribe();
    	$subscribe->email = $request->email;
    	$subscribe->save();

    	Toastr::success('Thanks For Subscribe Our Website', 'Success');
    	return redirect()->route('home');
    }
}

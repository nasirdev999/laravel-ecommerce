<?php

namespace App\Http\Controllers\Frontend;

use App\Brand;
use App\Contact;
use App\Category;
use App\CompanyLogo;
use App\CompanyInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ContactController extends Controller
{
    public function index()
    {
        $data['brands'] = Brand::latest()->get();
    	$data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
    	return view('frontend.contact.contacts',$data);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'subject' => 'required',
    		'email' => 'required',
    		'message' => 'required',
    	]);

    	$contact = new Contact();

    	$contact->name = $request->name;
    	$contact->subject = $request->subject;
    	$contact->email = $request->email;
    	$contact->message = $request->message;
    	$contact->save();

    	Toastr::success('Your message successfully sent.We are met very soon with you.','Success');
    	return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\Frontend;

use PDF;
use Mail;
use App\Brand;
use App\Order;
use App\Payment;
use App\Customar;
use App\Category;
use App\Checkout;
use App\CompanyInfo;
use App\CompanyLogo;
use App\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;


class CustomarController extends Controller
{
   	public function store(Request $request)
   	{
   		$this->validate($request,[
   			'name' => 'required',
   			'email' => 'required|unique:customars,email',
   			'password' => 'required',
   		]);

         $code = rand(0000, 9999);
   		$customar = new Customar();

   		$customar->name = $request->name;
   		$customar->email = $request->email;
   		$customar->password = md5($request->password);
         $customar->code = $code;
         $customar->status = '0';
   		$customar->save();

         $data = array(
            'email' => $request->email,
            'code' => $code,
         );

         Mail::send('frontend.customar.verifyEmail', $data, function($message) use($data){
            $message->from('likeproject12345@gmail.com','Smart Bazer');
            $message->to($data['email']);
            $message->subject('Smart Bazer Please your account verify by email address and Pin code.');
         });

   		Toastr::success('Your Account Registeration Successfull.Please your confirm your account','Success');
   		return redirect()->route('customar.verify');
   	}

      public function customarVerify()
      {
         $data['brands'] = Brand::latest()->get();
         $data['categoris'] = Category::latest()->get();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
         return view('frontend.customar.cusVerify',$data);
      }

      public function cusVerify(Request $request)
      {


         $custVerify = Customar::where('email',$request->email)
                        ->where('code',$request->code)
                        ->first();

         if($custVerify){

            $custVerify->status = '1';
            $custVerify->save();

            Toastr::success('Your Account Confirmation Successfull.Please Login Now','Success');
            return redirect()->route('home');
         }else{
            Toastr::error('Please Check Your Email & Pincode','Error');
            return redirect()->back();
         }

      }

   	public function cusLogin(Request $request)
   	{
   		$this->validate($request, [
   			'email' => 'required',
   			'password' => 'required',
   		]);

   		$email = $request->email;
   		$password = md5($request->password);
   		$customarLogin = Customar::where('email',$email)
						->where('password', $password)
                  ->where('status','1')
						->first();

		if($customarLogin){
			Session::put('cusID', $customarLogin->id);
			Session::put('cusName', $customarLogin->name);
			return redirect()->route('customar.cusdashboard');
		}else{
			Toastr::error('Your Email & Password Not Matching !!', 'Error');
			return redirect()->back();
		}		

   	}

   	public function cusLogout()
   	{
         $seID = session_id();
         Checkout::where('seID', $seID)->delete();
   		Session::flush();
   		return redirect()->route('home');
   	}

   	public function cusDashboard()
   	{
   		$cusID = Session::get('cusID');
         $data['brands'] = Brand::latest()->get();
         $data['categoris'] = Category::latest()->get();
   		$data['customar'] = Customar::where('id', $cusID)->first();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
   		return view('frontend.customar.cusDashboard',$data);
   	}

   	public function cusProfile()
   	{
   		$cusID = Session::get('cusID');
         $data['brands'] = Brand::latest()->get();
   		$data['customarEdit'] = Customar::where('id', $cusID)->first();
         $data['categoris'] = Category::latest()->get();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
   		return view('frontend.customar.cusProfile',$data);
   	}

   	public function cusProfileUpdate(Request $request)
   	{
   		$this->validate($request, [
   			'name' => 'required',
   			'email' => 'required',
   			'phone' => 'required',
   			'city' => 'required',
   			'address' => 'required',
   			'image' => 'mimes:jpg,jpeg,png',
   		]);

   		$cusID = Session::get('cusID');
   		$customar = Customar::where('id', $cusID)->first();
   		$customar->name = $request->name;
   		$customar->email = $request->email;
   		$customar->phone = $request->phone;
   		$customar->city = $request->city;
   		$customar->address = $request->address;

   		$image = $request->file('image');
   		if($image){
   			$imagename = str_slug($request->name).'-'.uniqid().'.'.$image->getClientOriginalExtension();
   			$image->move('upload/customarImage/',$imagename);
   			if(file_exists('upload/customarImage/'.$customar->image)){
   				unlink('upload/customarImage/'.$customar->image);
   			}
   			$customar->image = $imagename;
   		}

   		$customar->save();

   		Toastr::success('Your Profile Updated','Success');
   		return redirect()->route('customar.cusdashboard');
   	}

   	public function cusPassword()
   	{
   		$cusID = Session::get('cusID');
         $data['brands'] = Brand::latest()->get();
   		$data['customarPassword'] = Customar::where('id', $cusID)->first();
         $data['categoris'] = Category::latest()->get();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
   		return view('frontend.customar.cusPassword',$data);
   	}

   	public function cusPasswordUpdate(Request $request)
   	{
   		$this->validate($request, [
   			'old_password' => 'required',
   			'password' => 'required|confirmed',
   		]);

   		$old_pass = md5($request->old_password);
   		$new_pass = $request->password;

   		$cusID = Session::get('cusID');
   		$customarPassword = Customar::where('id', $cusID)->first();

   		if($customarPassword->password == $old_pass)
   		{
   			if($request->old_password != $new_pass)
   			{
   				$customarPassword->password = md5($request->password);
   				$customarPassword->update();
   				Session::flush();
   				Toastr::success('Your Password Updated !!','Success');
   				return redirect()->route('customar.cusdashboard');
   			}else{
   				Toastr::error('Old Password & New Password Matching !!','Error');
   				return redirect()->back();
   			}

   		}else{
   			Toastr::error('Your Old Password Not Exists !!','Error');
   			return redirect()->back();
   		}

   	}

      public function cusOrder()
      {
         $cusID = Session::get('cusID');
         $data['brands'] = Brand::latest()->get();
         $data['categoris'] = Category::latest()->get();
         $data['order_cus'] = Order::where('customar_id', $cusID)->get();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
         return view('frontend.customar.cusOrder',$data);
      }

      public function orderView($id)
      {
         $cusID = Session::get('cusID');
         $data['brands'] = Brand::latest()->get();
         $data['categoris'] = Category::latest()->get();
         $data['companyInfo'] = CompanyInfo::where('id',1)->first();
         $data['companyLogo'] = CompanyLogo::where('id',1)->first();
         $data['orderView'] = Order::where('id', $id)->where('customar_id',$cusID)->first();
         return view('frontend.customar.orderView',$data);
      }

      public function orderPdf($id)
      {
         $cusID = Session::get('cusID');
         $data['orderpdf'] = Order::where('id', $id)->where('customar_id',$cusID)->first();
         $pdf = PDF::loadView('frontend.customar.pdforder', $data);
         $pdf->SetProtection(['copy', 'print'], '', 'pass');
         return $pdf->stream('orderpdfile.pdf');
      }


}

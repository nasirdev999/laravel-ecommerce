<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use App\Brand;
use App\Product;
use App\Category;
use App\Checkout;
use App\CompanyLogo;
use App\CompanyInfo;
use App\ProductUnit;
use App\MadeCountry;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;
session_start();

class CheckoutController extends Controller
{
    public function index()
    {
    	$seID = session_id(); 
    	$data['alldata'] = Checkout::where('seID', $seID)->get();
        $data['brands'] = Brand::latest()->get();
        $data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
    	return view('frontend.pages.checkout',$data);
    }

    public function store(Request $request, $slug)
    {
    	$checkout = new Checkout();
    	$product = Product::where('slug', $slug)->first();
    	$seID = session_id();

    	$checkout->seID = $seID;
    	$checkout->product_id = $product->id;
    	$checkout->product_slug = $product->slug;
    	$checkout->product_name = $product->name;
    	$checkout->image = $product->image;

    	if(isset($request->quantity))
    	{
    		$checkout->quantity = $request->quantity;
    	}else{
    		$checkout->quantity = '1';
    	}
    	
    	$checkout->price = $product->discount_price;
    	$checkout->amount = $checkout->quantity * $checkout->price;
    	$checkout->save();

    	Toastr::success('Product Shop Success !!','Success');
    	return redirect()->route('checkout.index');
    }

    public function update(Request $request, $id)
    {

    	$checkoutUpdate = Checkout::find($id);

    	$checkoutUpdate->quantity = $request->quantity;
    	$checkoutUpdate->price = $checkoutUpdate->price;
    	$checkoutUpdate->amount = $checkoutUpdate->quantity * $checkoutUpdate->price;
    	$checkoutUpdate->update();

    	Toastr::success('Product Quantity Updated Success !!','Success');
    	return redirect()->route('checkout.index');
    }

    public function destroy($id)
    {
    	$checkoutDelete = Checkout::find($id)->delete();
    	return response()->json($checkoutDelete);
    }

}

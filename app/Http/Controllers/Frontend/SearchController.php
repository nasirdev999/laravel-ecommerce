<?php

namespace App\Http\Controllers\Frontend;

use App\Brand;
use App\Product;
use App\Category;
use App\CompanyInfo;
use App\CompanyLogo;
use App\MadeCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class SearchController extends Controller
{
    public function searchProduct(Request $request)
    {
    	$query = $request->Search;
    	$data['brands'] = Brand::latest()->get();
    	$data['specialProduct'] = Product::take(4)->get();
        $data['categoris'] = Category::latest()->get();
        $data['madeCountry'] = MadeCountry::latest()->get();
		$data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
    	$data['products'] = Product::where('name','LIKE','%$query%')->get();
    	return view('frontend.search.search',$data);
    }
}

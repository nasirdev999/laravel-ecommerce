<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Auth;
use App\Unit;
use App\Brand;
use App\Color;
use App\Product;
use App\Category;
use App\Checkout;
use App\ProductUnit;
use App\MadeCountry;
use App\Order;
use App\Payment;
use App\OrderDetail;
use App\SubCategory;
use App\ProductColor;
use App\ProductSubImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;



class OrderController extends Controller
{
    public function storeByCash(Request $request)
    {
    	$this->validate($request, [
    		'cash_pay' => 'required'
    	]);

    	DB::transaction(function() use($request){

    		$cusID = Session::get('cusID');
    		$shipID = Session::get('shipID');

    		$payment = new Payment();
	    	$payment->payment_method = 'cash_payment';
	    	
	    	if($payment->save()){

	    		$order_data = Order::orderBy('id','desc')->first();
    			if($order_data == null){
    				$first_no = '0';
    				$order_no = $first_no + 1;
    			}else{
    				$order_data = Order::orderBy('id','desc')->first()->order_no;
    				$order_no = $order_data + 1;
    			}


	    		$order = new Order();
	    		$order->order_date = $request->order_date;
	    		$order->customar_id = $cusID;
	    		$order->shipping_id = $shipID;
	    		$order->payment_id = $payment->id;
	    		$order->order_no = $order_no;
	    		$order->order_total = $request->cash_pay;
	    		$order->status = '0';
	    		$order->save();

	    		$seID = session_id();
	    		$checkouts = Checkout::where('seID', $seID)->get();
	    		foreach($checkouts as $checkout){
	    			$checkoutAdd = new OrderDetail();
	    			$checkoutAdd->order_id = $order->id;
	    			$checkoutAdd->product_id = $checkout->product_id;
	    			$checkoutAdd->quantity = $checkout->quantity;
	    			$checkoutAdd->save();
	    		}
	    		Checkout::where('seID', $seID)->delete();

	    	}

    	});

    	Toastr::success('Order Successfull','Success');
    	return redirect()->route('customar.cusdashboard');

    }

    public function storeBybkash(Request $request)
    {
    	$this->validate($request, [
    		'cash_pay' => 'required',
    		'phone' => 'required',
    		'transaction_no' => 'required',
    	]);

    	DB::transaction(function() use($request){

    		$cusID = Session::get('cusID');
    		$shipID = Session::get('shipID');

    		$payment = new Payment();
	    	$payment->payment_method = 'bkash_payment';
	    	$payment->phone = $request->phone;
	    	$payment->transaction_no = $request->transaction_no;
	    	
	    	if($payment->save()){

	    		$order_data = Order::orderBy('id','desc')->first();
    			if($order_data == null){
    				$first_no = '0';
    				$order_no = $first_no + 1;
    			}else{
    				$order_data = Order::orderBy('id','desc')->first()->order_no;
    				$order_no = $order_data + 1;
    			}


	    		$order = new Order();
	    		$order->order_date = $request->order_date;
	    		$order->customar_id = $cusID;
	    		$order->shipping_id = $shipID;
	    		$order->payment_id = $payment->id;
	    		$order->order_no = $order_no;
	    		$order->order_total = $request->cash_pay;
	    		$order->status = '0';
	    		$order->save();

	    		$seID = session_id();
	    		$checkouts = Checkout::where('seID', $seID)->get();
	    		foreach($checkouts as $checkout){
	    			$checkoutAdd = new OrderDetail();
	    			$checkoutAdd->order_id = $order->id;
	    			$checkoutAdd->product_id = $checkout->product_id;
	    			$checkoutAdd->quantity = $checkout->quantity;
	    			$checkoutAdd->save();
	    		}
	    		Checkout::where('seID', $seID)->delete();

	    	}

    	});

    	Toastr::success('Order Successfull. We are check your payment and contact with you.','Success');
    	return redirect()->route('customar.cusdashboard');

    }

   

}

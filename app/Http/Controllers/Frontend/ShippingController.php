<?php

namespace App\Http\Controllers\Frontend;

use Session;
use App\Shipping;
use App\Brand;
use App\Category;
use App\CompanyInfo;
use App\CompanyLogo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class ShippingController extends Controller
{
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'phone' => 'required',
    		'city' => 'required',
    		'address' => 'required',
    	]);

    	$shipping = new Shipping();
    	$shipping->name = $request->name;
    	$shipping->phone = $request->phone;
    	$shipping->city = $request->city;
    	$shipping->address = $request->address;

    	$shipping->save();
    	Session::put('shipID',$shipping->id);
    	Toastr::success('Please give me your payment and take it your product','Success');
    	return redirect()->route('customar.payment');
    }

    public function payment()
    {
        $data['brands'] = Brand::latest()->get();
    	$data['categoris'] = Category::latest()->get();
        $data['companyInfo'] = CompanyInfo::where('id',1)->first();
        $data['companyLogo'] = CompanyLogo::where('id',1)->first();
    	return view('frontend.pages.payment',$data);
    }
}

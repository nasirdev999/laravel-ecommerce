<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
    	return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function subCategory()
    {
    	return $this->belongsTo(SubCategory::class, 'SubCategory_id', 'id');
    }

    public function brand()
    {
    	return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    public function madeCountry()
    {
    	return $this->belongsTo(MadeCountry::class, 'MadeCountry_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }

    public function scopeAproved($query)
    {
        return $query->where('is_approve', 1);
    }
}


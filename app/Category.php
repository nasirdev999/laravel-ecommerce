<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function subCategoris()
    {
    	return $this->hasMany(SubCategory::class, 'category_id', 'id');
    }
}

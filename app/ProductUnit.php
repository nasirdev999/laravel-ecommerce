<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    public function units()
    {
    	return $this->belongsTo(Unit::class, 'units_id', 'id');
    }
}

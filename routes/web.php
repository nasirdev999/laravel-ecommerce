<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace'=>'Frontend'],function(){

	Route::get('/','HomePageController@homePageData')->name('home');

	// product details, category wise product, brand wise product, country wise product 
	Route::get('/product-details/{slug}','ProductDetailsController@productDetails')->name('product.details');
	Route::get('/category-wise/product/{slug}','ProductDetailsController@categoryByPro')->name('product.categorywise.data');
	Route::get('/brand-wise/product/{slug}','ProductDetailsController@brandByPro')->name('product.brandwise.data');
	Route::get('/country-wise/product/{slug}','ProductDetailsController@countryByPro')->name('product.countrywise.data');
	Route::get('/all-products/product','ProductDetailsController@allproduct')->name('product.allproducts.data');

	//checkout routes
	Route::get('/checkout','CheckoutController@index')->name('checkout.index');
	Route::post('/checkout-store/{slug}','CheckoutController@store')->name('checkout.store');
	Route::post('/checkout-update/{id}','CheckoutController@update')->name('checkout.update');
	Route::get('/checkout-delete/{id}','CheckoutController@destroy')->name('checkout.destroy');

	//customar routes
	Route::get('customar/dashboard','CustomarController@cusDashboard')->name('customar.cusdashboard');
	Route::get('customar/profile','CustomarController@cusProfile')->name('customar.profile');
	Route::post('customar/profile-update','CustomarController@cusProfileUpdate')->name('customar.profile.update');
	Route::get('customar/password-change','CustomarController@cusPassword')->name('customar.password');
	Route::post('customar/password-change/update','CustomarController@cusPasswordUpdate')->name('customar.password.update');
	Route::post('customar/store','CustomarController@store')->name('customar.store');
	Route::get('customar/login','CustomarController@cusLogin')->name('customar.cuslogin');
	Route::get('customar/logout','CustomarController@cusLogout')->name('customar.logout');
	Route::get('customar/order','CustomarController@cusOrder')->name('customar.order');
	Route::get('/order-view/{id}','CustomarController@orderView')->name('customar.order.view');
	Route::get('/order-view/pdf/{id}','CustomarController@orderPdf')->name('customar.order.list.pdf');
	//customar verify
	Route::get('customar-account/verify','CustomarController@customarVerify')->name('customar.verify');
	Route::post('customar/verify-success','CustomarController@cusVerify')->name('customar.verify.completes');

	// shipping and payment 
	Route::post('/shipping-store','ShippingController@store')->name('shipping.store');
	Route::get('/payment','ShippingController@payment')->name('customar.payment');

	// contact routes
	Route::get('/contact','ContactController@index')->name('contact.index');
	Route::post('/contact-store','ContactController@store')->name('contact.store');

	// subscriber routes
	Route::post('/subscribe','SubscribeController@store')->name('subscribe.store');

	// order routes
	Route::post('/order-store/by-cash','OrderController@storeByCash')->name('order.store.by.cash');
	Route::post('/order-store/by-bkash','OrderController@storeBybkash')->name('order.store.by.bkash');

	// contact routes
	Route::get('/search-products','SearchController@searchProduct')->name('search.index');

	// default routes | about, faqs, helps, 
	Route::get('/abouts','DefaultController@aboutPages')->name('about.page');
	Route::get('/faqs','DefaultController@faqsPages')->name('faqs.page');
	Route::get('/helps','DefaultController@helpPages')->name('help.page');
	Route::get('/terms-of-use','DefaultController@termsPages')->name('termsofuse.page');
	Route::get('/privacy-policy','DefaultController@privacyPages')->name('privacy.page');


});






Auth::routes();


Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin', 'middleware'=>['auth','admin']],function(){

	Route::get('dashboard','AdminDashboardController@index')->name('dashboard');

	//brand proccess routes ajax
	Route::get('brand/list','BrandController@index')->name('brand.index');
	Route::get('brand/alldata','BrandController@allData')->name('brand.alldata');
	Route::post('brand/store','BrandController@store')->name('brand.store');
	Route::get('brand/edit/{id}','BrandController@edit')->name('brand.edit');
	Route::post('brand/update/{id}','BrandController@update')->name('brand.update');
	Route::get('brand/destroy/{id}','BrandController@destroy')->name('brand.destroy');
	//brand proccess by ajax routes 

	Route::resource('category','CategoryController');
	Route::resource('subcategory','SubCategoryController');
	Route::resource('units','Unitcontroller');
	Route::resource('made-country','MadeCountryController');
	Route::resource('color','ColorController');
	Route::resource('slider','SliderController');
	Route::resource('banner','AdBannerController');
	
	//Product controller
	Route::get('product/list','ProductsController@index')->name('products.index');
	Route::get('product/create','ProductsController@index')->name('products.create');
	Route::get('product/show/{id}','ProductsController@show')->name('products.show');
	Route::get('product/edit/{id}','ProductsController@edit')->name('products.edit');
	Route::put('products/update/{id}','ProductsController@update')->name('products.update');
	Route::delete('product/destroy/{id}','ProductsController@destroy')->name('products.destroy');
	Route::get('product/approved/{id}','ProductsController@approvedProduct')->name('product.approved');
	Route::get('product/unapproved/{id}','ProductsController@unApprovedProduct')->name('product.unapproved');
	Route::get('product/pending-list','ProductsController@pendingProduct')->name('products.pending.list');


	//order routes
	Route::get('order/pending-list','OrderController@orderPendingList')->name('order.pending');
	Route::get('order/approve-list','OrderController@orderApproveList')->name('order.approve');
	Route::get('order/approval/{id}','OrderController@orderApproval')->name('order.approval.check');
	Route::post('order/pproval/{id}','OrderController@orderApproveUpdate')->name('order.approval.update');

	//customar routes
	Route::get('customar/list','CustomarController@index')->name('customar.index');
	Route::get('customar/unverify-list','CustomarController@unverifyList')->name('customar.unverify.list');
	Route::delete('customar/destroy/{id}','CustomarController@customarDestroy')->name('customar.destroy');

	//contact routes
	Route::get('/contact/message-seen','ContactController@seenList')->name('contact.seen.list');
	Route::get('/contact/message-unseen','ContactController@uneenList')->name('contact.unseen.list');
	Route::get('/contact/message-unseen/view/{id}','ContactController@uneenListView')->name('contact.unseen.view.list');
	Route::post('/contact/message-update/view/{id}','ContactController@uneenUpdate')->name('contact.update.list');
	Route::get('/contact/message-delete/{id}','ContactController@messageDelete')->name('contact.delete.list');
	Route::get('/contact/message/reply/{id}','ContactController@messageReply')->name('contact.reply');
	Route::get('/contact/message/reply/send','ContactController@contactReply')->name('contact.reply.send');

	//company information routes
	Route::get('/company-information','CompanyInfoControler@index')->name('company.info.index');
	Route::get('/company-information/{id}','CompanyInfoControler@edit')->name('company.info.edit');
	Route::post('/company-information/update/{id}','CompanyInfoControler@update')->name('company.info.update');

	//company logo routes
	Route::get('/company-logo','CompanyInfoControler@logoIndex')->name('company.logo.index');
	Route::get('/company-logo/{id}','CompanyInfoControler@logoEdit')->name('company.logo.edit');
	Route::post('/company-logo/update/{id}','CompanyInfoControler@logoUpdate')->name('company.logo.update');

	// subscriber routes
	Route::get('/subscribe','SubsribeController@index')->name('subscribe.index');

	// about routes
	Route::get('/about','AboutController@aboutIndex')->name('about.index');
	Route::get('/about-update/{id}','AboutController@edit')->name('about.edit');
	Route::post('/about-update/{id}','AboutController@aboutUpdate')->name('about.update');

	//user routes
	Route::get('user/','UserController@index')->name('user.index');
	Route::post('user/store','UserController@store')->name('user.store');
	Route::get('user/edit/{id}','UserController@edit')->name('user.edit');
	Route::post('user/update/{id}','UserController@update')->name('user.update');

	// user setting routes
	Route::get('profile/setting','SettingController@userProfile')->name('user.setting.profile');
	Route::post('profile/setting/update/{id}','SettingController@userUpdate')->name('user.setting.profile.update');
	Route::get('password/setting','SettingController@userPassword')->name('user.setting.password');
	Route::post('password/setting/update','SettingController@userPasswordUpdate')->name('user.setting.password.update');


	// default controller 
	Route::get('get-subcategory','DefaultController@getSubCateogory')->name('get.subcategory');

});

Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author', 'middleware'=>['auth','author']],function(){

	Route::get('dashboard','AuthorDashboardController@index')->name('dashboard');

	//Product controller
	Route::resource('product','ProductController');
	
	//order routes
	Route::get('order/pending-list','OrderController@orderPendingList')->name('order.pending');
	Route::get('order/approve-list','OrderController@orderApproveList')->name('order.approve');
	Route::get('order/approval/{id}','OrderController@orderApproval')->name('order.approval.check');
	Route::post('order/pproval/{id}','OrderController@orderApproveUpdate')->name('order.approval.update');

	//customar routes
	Route::get('customar/list','CustomarController@index')->name('customar.index');
	Route::get('customar/unverify-list','CustomarController@unverifyList')->name('customar.unverify.list');
	Route::delete('customar/destroy/{id}','CustomarController@customarDestroy')->name('customar.destroy');

	// user setting routes
	Route::get('profile/setting','SettingController@userProfile')->name('user.setting.profile');
	Route::post('profile/setting/update/{id}','SettingController@userUpdate')->name('user.setting.profile.update');
	Route::get('password/setting','SettingController@userPassword')->name('user.setting.password');
	Route::post('password/setting/update','SettingController@userPasswordUpdate')->name('user.setting.password.update');

	// default controller 
	Route::get('get-subcategory','DefaultController@getSubCateogory')->name('get.subcategory');

});

Route::group(['as'=>'editor.','prefix'=>'editor','namespace'=>'Editor', 'middleware'=>['auth','editor']],function(){

	Route::get('dashboard','EditorDashboardController@index')->name('dashboard');

	//order routes
	Route::get('order/pending-list','OrderController@orderPendingList')->name('order.pending');
	Route::get('order/approve-list','OrderController@orderApproveList')->name('order.approve');
	Route::get('order/approval/{id}','OrderController@orderApproval')->name('order.approval.check');
	Route::post('order/pproval/{id}','OrderController@orderApproveUpdate')->name('order.approval.update');

	//customar routes
	Route::get('customar/list','CustomarController@index')->name('customar.index');
	Route::get('customar/unverify-list','CustomarController@unverifyList')->name('customar.unverify.list');
	Route::delete('customar/destroy/{id}','CustomarController@customarDestroy')->name('customar.destroy');


	// user setting routes
	Route::get('profile/setting','SettingController@userProfile')->name('user.setting.profile');
	Route::post('profile/setting/update/{id}','SettingController@userUpdate')->name('user.setting.profile.update');
	Route::get('password/setting','SettingController@userPassword')->name('user.setting.password');
	Route::post('password/setting/update','SettingController@userPasswordUpdate')->name('user.setting.password.update');


	
});

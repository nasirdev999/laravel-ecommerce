<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Rakib Chowdhury',
            'userType' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('rootadmin'),
        ]);

        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Hasan Ali',
            'userType' => 'author',
            'email' => 'author@gmail.com',
            'password' => bcrypt('rootauthor'),
        ]);

         DB::table('users')->insert([
            'role_id' => '3',
            'name' => 'Nila Akter',
            'userType' => 'editor',
            'email' => 'editor@gmail.com',
            'password' => bcrypt('rooteditor'),
        ]);
    }
}

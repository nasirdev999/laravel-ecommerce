<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id');
            $table->integer('category_id');
            $table->integer('SubCategory_id');
            $table->integer('MadeCountry_id');
            $table->string('title')->unique();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('image')->default('default.png');
            $table->text('short_details')->nullable();
            $table->longText('long_details')->nullable();
            $table->integer('stock')->nullable();
            $table->float('buying_price',8,2)->nullable();
            $table->float('selling_price',8,2)->nullable();
            $table->integer('discount_percent')->nullable();
            $table->float('discount_price',8,2)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('is_approve')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

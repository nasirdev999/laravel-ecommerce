@extends('backend.master')

@section('title')
	Country update 
@endsection

@section('heading')
	Country Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Country Updated</h3>
	          <a href="{{ route('admin.category.index') }}" type="button" class="btn btn-success float-right"><i class="fa fa-list"></i> Country List</a>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	          	<form action="{{ route('admin.made-country.update',$editData->id) }}" method="post" id="myForm">
	          		@csrf
	          		@method('PUT')
	          		<div class="form-row">
	          			<div class="form-group col-md-4">
	          				<label for="">Country Name </label>
	          				<input type="text" name="name" id="name" class="form-control" value="{{ $editData->name }}">
	          			</div>
	          		</div>
          			<div class="form-group col-md-2">
          				<button type="submit" class="btn btn-primary">Update</button>
          			</div>
	          	</form>
	      	</div>
    	</div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      }
	    },
	    messages: {
	      name: {
	        required: "Please enter a name address",
	        name: "Please enter a vaild name address"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>

</section>

@endsection
@extends('backend.master')

@section('title')
	About Us update
@endsection

@section('heading')
	About Us Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">About Us Update</h3>
          <a href="{{ route('admin.about.index') }}" class="btn btn-success float-right"><i class="fa fa-list"></i> List About</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        	<form action="{{ route('admin.about.update',$editData->id) }}" method="post" enctype="multipart/form-data">
        		@csrf
        		<div class="form-group">
	              	<label for="name">Image One </label>
	              	<input type="file" name="image1" id="image1" class="form-control">
	              	<img src="{{ asset('upload/about/'.$editData->image1) }}" alt="" height="50px" width="50px">
	            </div>
	            <div class="form-group">
	              	<label for="name">Image Two </label>
	              	<input type="file" name="image2" id="image2" class="form-control">
	              	<img src="{{ asset('upload/about/'.$editData->image2) }}" alt="" height="50px" width="50px">
	            </div>
	            <div class="form-group">
	              	<label for="name">Video </label>
	              	<input type="text" name="video" id="video" class="form-control" value="{{ $editData->video }}">
	            </div>
	            <div class="form-group">
	              	<label for="name">Details </label>
	              	<textarea name="details" class="textarea" placeholder="Place some text here"
                        style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $editData->details }}</textarea>
	            </div>
	            <div class="form-group">
	            	<input type="submit" class="btn btn-primary">
	            </div>
	        </form>  
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
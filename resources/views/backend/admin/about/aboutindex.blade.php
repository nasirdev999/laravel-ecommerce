@extends('backend.master')

@section('title')
	About Us
@endsection

@section('heading')
	About Us Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">About Us List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Image One</th>
                <th>Image Two</th>
                <th>Vedio</th>
                <th>Details</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$about)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>
        				<img src="{{ asset('upload/about/'.$about->image1) }}" alt="" height="50px" width="50px">
        			</td>
        			<td>
        				<img src="{{ asset('upload/about/'.$about->image2) }}" alt="" height="50px" width="50px">
        			</td>
        			<td>{{ $about->video }}</td>
        			<td>{{ $about->details }}</td>
        			<td>
        				<a title="Edit" href="{{ route('admin.about.edit',$about->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
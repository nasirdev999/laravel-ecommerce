@extends('backend.master')

@section('title')
	Slider update 
@endsection

@section('heading')
	Slider Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Slider Updated</h3>
	          <a href="{{ route('admin.slider.index') }}" type="button" class="btn btn-success float-right"><i class="fa fa-list"></i> Slider List</a>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	          	<form action="{{ route('admin.slider.update',$editData->id) }}" method="post" id="myForm" enctype="multipart/form-data">
	          		@csrf
	          		@method('PUT')
	          		<div class="form-row">
	          			<div class="form-group">
			              	<label for="name">Heading 1 </label>
			              	<input type="text" name="heading1" id="name" class="form-control" value="{{ $editData->heading1 }}">
			            </div>
			            <div class="form-group">
			              	<label for="name">Heading 2 </label>
			              	<input type="text" name="heading2" id="name" class="form-control" value="{{ $editData->heading2 }}">
			            </div>
			            <div class="form-group">
			              	<label for="name">Pragraph one </label>
			              	<input type="text" name="para1" id="name" class="form-control" value="{{ $editData->para1 }}">
			            </div>
			            <div class="form-group">
			              	<label for="name">Pragraph two </label>
			              	<input type="text" name="para2" id="name" class="form-control" value="{{ $editData->para2 }}">
			            </div>
			            <div class="form-group">
			              	<label for="name">Discount </label>
			              	<input type="text" name="discount" id="name" class="form-control" value="{{ $editData->discount }}">
			            </div>
			            <div class="form-group">
			              	<label for="name">Image </label>
			              	<input type="file" name="image" id="name" class="form-control" >
			              	<img src="{{ asset('upload/sliderImage/'.$editData->image) }}" alt="" height="70px" width="70px">
			            </div>
	          		</div>
          			<div class="form-group col-md-2">
          				<button type="submit" class="btn btn-primary">Update</button>
          			</div>
	          	</form>
	      	</div>
    	</div>
    </div>
</div>



</section>

@endsection
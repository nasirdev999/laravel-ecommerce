@extends('backend.master')

@section('title')
	Slider
@endsection

@section('heading')
	Slider Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Slider List</h3>
          <button onclick="formShow()" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Add Slider</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Heading 1</th>
                <th>Heading 2</th>
                <th>Para 1</th>
                <th>Para 2</th>
                <th>Discount</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$slider)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $slider->heading1 }}</td>
        			<td>{{ $slider->heading2 }}</td>
        			<td>{{ $slider->para1 }}</td>
        			<td>{{ $slider->para2 }}</td>
        			<td>{{ $slider->discount }}</td>
        			<td>
        				<img src="{{ asset('upload/sliderImage/'.$slider->image) }}" alt="" height="70px" width="70px">
        			</td>
        			<td>
        				<a title="Edit" href="{{ route('admin.slider.edit',$slider->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <button onclick="deleteData({{ $slider->id }})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
        				<form action="{{ route('admin.slider.destroy',$slider->id) }}" id="delete-form-{{ $slider->id}}" method="post">
                  @csrf
                  @method('DELETE')    
                </form>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
          	<form action="{{ route('admin.slider.store') }}" id="myForm" method="post" enctype="multipart/form-data">
          		@csrf
	            <div class="modal-header">
	              <h4 class="modal-title">Slider Created ...</h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <div class="form-group">
	              	<label for="name">Heading 1 </label>
	              	<input type="text" name="heading1" id="name" class="form-control" placeholder="Heading one">
	              </div>
	              <div class="form-group">
	              	<label for="name">Heading 2 </label>
	              	<input type="text" name="heading2" id="name" class="form-control" placeholder="Heading two">
	              </div>
	              <div class="form-group">
	              	<label for="name">Pragraph one </label>
	              	<input type="text" name="para1" id="name" class="form-control" placeholder="Paragraph One ">
	              </div>
	              <div class="form-group">
	              	<label for="name">Pragraph two </label>
	              	<input type="text" name="para2" id="name" class="form-control" placeholder="Paragraph two">
	              </div>
	              <div class="form-group">
	              	<label for="name">Discount </label>
	              	<input type="text" name="discount" id="name" class="form-control" placeholder="Discount">
	              </div>
	              <div class="form-group">
	              	<label for="name">Image </label>
	              	<input type="file" name="image" id="name" class="form-control" >
	              </div>
	            </div>
	            <div class="modal-footer justify-content-between">
	              <button id="saveButton" type="submit" class="btn btn-primary">Save</button>
	            </div>
	          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->

</div>
</section>

<script type="text/javascript">
	function formShow(){
		$('#myModal').modal('show');
	}

  function deleteData(id){
    document.getElementById('delete-form-'+id).submit();
  }
	
</script>


@endsection
@extends('backend.master')

@section('title')
	order pending
@endsection

@section('heading')
	Order Pending
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Order Pending</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Date</th>
                <th>Order No</th>
                <th>Customar Name</th>
                <th>Order Amount</th>
                <th>status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$or_pending)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $or_pending->order_date }}</td>
        			<td>{{ $or_pending->order_no }}</td>
        			<td>{{ $or_pending->customar->name }}</td>
        			<td>{{ $or_pending->order_total }}</td>
        			<td>
        				@if($or_pending->status == false)
        					<span class="badge badge-danger">Pending</span>
        				@endif
        			</td>
        			<td>
        				<a title="Approve" href="{{ route('admin.order.approval.check',$or_pending->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
@extends('backend.master')

@section('title')
	pending	product 
@endsection

@section('heading')
	Product Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Product Pending List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped product">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Author</th>
                <th>Name</th>
                <th>Image</th>
                <th>Status</th>
                <th>Is Approve</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($pendingData as $key=>$product)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $product->user->userType }}</td>
        			<td>{{ $product->name }}</td>
        			<td><img src="{{ asset('upload/productImage/'.$product->image) }}" alt="" height="50px" width="50px"></td>
        			<td>
        				@if($product->status == true)
        					<span class="badge badge-success">Active</span>
        				@else
        					<span class="badge badge-danger">Pending</span>
        				@endif		
        			</td>
                     <td>
                        @if($product->is_approve == true)
                            <span class="badge badge-info">Approved</span>
                        @else
                            <span class="badge badge-warning">Pending</span>
                        @endif      
                    </td>
        			<td>

        				@if($product->is_approve == true)
        				<button id="unApprovebtn" onclick="productUnapprove({{ $product->id }})" title="Approved" class="btn btn-sm btn-primary"><i class="fa fa-thumbs-up"></i></button>
        				@else
        				<button id="approvebtn" onclick="productApprove({{ $product->id }})" id="approveButton" title="Is Approve" class="btn btn-sm btn-warning"><i class="fa fa-thumbs-down"></i></button>
        				@endif

        				<a title="View" href="{{ route('admin.products.show',$product->id) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
        				<a title="Edit" href="{{ route('admin.products.edit',$product->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				<button title="Delete" onclick="deleteData({{ $product->id }})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
        				<form action="{{ route('admin.products.destroy',$product->id) }}" method="post" id="form-delete-{{ $product->id }}">
        					@csrf
        					@method('DELETE')
        				</form>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>
</div>
</section>
<script>
	function deleteData(id){
		document.getElementById('form-delete-'+id).submit();
	}

	function productApprove(id){

		$.ajax({
			type:'get',
			dataType:'json',
			url:'/admin/product/approved/'+id,
			success:function(data){
				toastr.success('Product Approved Successfull', 'Success');
			},
			error:function(error){
				toastr.error('error','Error');
			}
		});

	}

	function productUnapprove(id){

		$.ajax({
			type:'get',
			dataType:'json',
			url:'/admin/product/unapproved/'+id,
			success:function(data){
				toastr.success('Product Unapproved Successfull', 'Success');
			},
			error:function(error){
				toastr.error('error','Error');
			}
		});

	}

</script>
@endsection
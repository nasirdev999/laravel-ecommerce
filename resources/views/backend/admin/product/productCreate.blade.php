@extends('backend.master')

@section('title')
	Product create
@endsection

@section('heading')
	Product Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Product Create</h3>
          <a href="{{ route('admin.products.index') }}" class="btn btn-success float-right"><i class="fa fa-list" ></i> List Product</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
         	<form action="{{ route('admin.products.store') }}" method="post" id="myForm" enctype="multipart/form-data">
         		@csrf
         		<div class="form-row">
         			<div class="form-group col-md-3">
         				<label for="">Name</label>
         				<input type="text" name="name" class="form-control" placeholder="Product Name">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Title</label>
         				<input type="text" name="title" class="form-control" placeholder="Product Title">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Category</label>
         				<select name="category_id" id="category_id" class="form-control select2bs4">
         					<option value="">Select Category</option>
         					@foreach($categoris as $category)
         						<option value="{{ $category->id }}">{{ $category->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Sub Category</label>
         				<select name="subCategory_id" id="subCategory_id" class="form-control select2bs4">
         					<option value="">Select Sub Category</option>
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Brand</label>
         				<select name="brand_id" id="" class="form-control select2bs4">
         					<option value="">Select Brand</option>
         					@foreach($brands as $brand)
         						<option value="{{ $brand->id }}">{{ $brand->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Color </label>
         				<select name="color_id[]" id="" class="form-control select2bs4" multiple="" data-placeholder="Select Color">
         					<option value="">Select Color</option>
         					@foreach($colors as $color)
         						<option value="{{ $color->id }}">{{ $color->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Units </label>
         				<select name="units_id[]" id="" class="form-control select2bs4" multiple="" data-placeholder="Select Units">
         					<option value="">Select Units</option>
         					@foreach($units as $unit)
         						<option value="{{ $unit->id }}">{{ $unit->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Country </label>
         				<select name="MadeCountry_id" id="" class="form-control select2bs4">
         					<option value="">Select Country</option>
         					@foreach($countris as $country)
         						<option value="{{ $country->id }}">{{ $country->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Buying Price</label>
         				<input type="number" name="buying_price" class="form-control" placeholder="Buying Price">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Selling Price</label>
         				<input type="number" name="selling_price" class="form-control" placeholder="Selling Price">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Discount Percent</label>
         				<input type="number" name="discount_percent" class="form-control" placeholder="Discount Percent">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Product Stock</label>
         				<input type="number" name="stock" class="form-control" placeholder="Product Stock">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Image </label>
         				<input type="file" name="image">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Multiple Sub Image </label>
         				<input type="file" name="sub_image[]" multiple="">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Is Approve </label>
         				<select name="is_approve" id="" class="form-control">
         					<option value="">Select Approval</option>
         					<option value="0">Pending</option>
         					<option value="1">Approve</option>
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<input type="checkbox" name="status" value="1">	
         				<label for=""> Product Status </label>
         			</div>
         		</div>
         		<div class="form-row">
         			<div class="form-group col-md-6">
         				<label for="">Short Description </label>
         				<textarea name="short_details" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
         			</div>
         			<div class="form-group col-md-6">
         				<label for="">Long Description </label>
         				<textarea name="long_details" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
         			</div>
         		</div>
         		<div class="form-group">
         			<button class="btn btn-primary">Save</button>
         		</div>
         	</form>
      	</div>
    	</div>
    </div>
</div>
</section>

<script>
	$(document).on('change','#category_id', function(){
		let category_id = $(this).val();

		$.ajax({
			type:'get',
			dataType:'json',
			data:{category_id:category_id},
			url:'{{ route("admin.get.subcategory") }}',
			success:function(data){
				let html = '<option value="">Select Sub Category</option>'
				$.each(data, function(key, value){
					html += '<option value="'+value.id+'">'+value.name+'</option>'
				});
				$('#subCategory_id').html(html);
			}
		});

	});	
</script>

<script type="text/javascript">


	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      title: {
	        required: true,
	        title: true,
	      },
	      category_id: {
	        required: true,
	        category_id: true,
	      },
	      brand_id: {
	        required: true,
	        brand_id: true,
	      },
	      MadeCountry_id: {
	        required: true,
	        MadeCountry_id: true,
	      },
	      buying_price: {
	        required: true,
	        buying_price: true,
	      },
		  selling_price: {
	        required: true,
	        selling_price: true,
	      },
	      stock: {
	        required: true,
	        stock: true,
	      },
	      image: {
	        required: true,
	        image: true,
	      },
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      title: {
	        required: "Please enter a title",
	        title: "Please enter a vaild title"
	      },
	      category_id: {
	        required: "Please enter a category",
	        category_id: "Please enter a vaild category"
	      },
	      brand_id: {
	        required: "Please enter a brand",
	        brand_id: "Please enter a vaild brand"
	      },
	      MadeCountry_id: {
	        required: "Please enter a MadeCountry",
	        MadeCountry_id: "Please enter a vaild MadeCountry"
	      },
	      buying_price: {
	        required: "Please enter a buying price",
	        buying_price: "Please enter a vaild buying price"
	      },
	      selling_price: {
	        selling_price: "Please enter a selling price",
	        selling_price: "Please enter a vaild selling price"
	      },
	      stock: {
	        required: "Please enter a stock",
	        stock: "Please enter a vaild stock"
	      },
	      image: {
	        required: "Please enter a image",
	        image: "Please enter a vaild image"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>

@endsection
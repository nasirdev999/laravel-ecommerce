@extends('backend.master')

@section('title')
	Brand
@endsection

@section('heading')
	Brand Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
	<div class="card">
        <div class="card-header">
          <h3 class="card-title">Brand List</h3>
          <button onclick="fromShow()" type="button" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Add Brand</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="brandData">
            	
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

    	<div class="modal fade" id="brandModal">
        <div class="modal-dialog">
          <div class="modal-content">
          	<form action="" id="brandFrom">
	            <div class="modal-header">
	              <h4 class="modal-title"></h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <div class="form-group">
	              	<label for="name">Brand Name </label>
	              	<input type="text" name="name" id="name" class="form-control" placeholder="Brand Name">
	              	<input type="hidden" id="id">
	              	<span style="color:red" id="nameError"></span>
	              </div>
	            </div>
	            <div class="modal-footer justify-content-between">
	              <button id="saveButton" type="button" class="btn btn-primary">Save</button>
	              <button id="updateButton" type="button" class="btn btn-primary">Update</button>
	            </div>
	          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

  </div>
</section>

<script>

	function fromShow(){
		$('.modal-title').text('Brand Created ...');
		$('#brandModal').modal('show');
		$('#name').val('');
		$('#saveButton').show();
		$('#updateButton').hide();
	}

	$.ajaxSetup({
    	headers: {
	      	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	function clearData(){
		$('#name').val('');
		$('#id').val();
		$('#nameError').text('');
	}

	//<===============brand read==============>

	function alldata(){

		$.ajax({
				type:'get',
				dataType:'json',
				url:'{{ route("admin.brand.alldata") }}',
				success:function(data){
					let html = '';
					let sl = 1;
					$.each(data, function(key, value){
						html += "<tr>"
								+"<td>"+(key+sl)+"</td>"
								+"<td>"+value.name+"</td>"
								+"<td>"+value.slug+"</td>"
								+"<td>"
								+"<button title='Edit' onclick='editData("+value.id+")' class='btn btn-sm btn-info'><i class='fa fa-edit'></i></button> "
								+"<button title='Delete' onclick='deleteData("+value.id+")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></button>"
								+"</td>"
								+"</tr>"
					});
					$('#brandData').html(html);
				}
		});

	}
	alldata();

//<===============brand add ==============>

	$(document).on('click','#saveButton', function(){
			let name = $('#name').val();
			$.ajax({
				type:'post',
				dataType:'json',
				data:{name:name},
				url:'{{ route("admin.brand.store") }}',
				success:function(data){
					toastr.success('Success','Brand Created Successfull');
					$('#brandModal').modal('hide');
					alldata();
					clearData();
				},
				error:function(error){
					$('#nameError').text(error.responseJSON.errors.name)
				}
			});
	});

	//<===============brand edit ==============>

	function editData(id){

		$.ajax({
				type:'get',
				dataType:'json',
				url:'/admin/brand/edit/'+id,
				success:function(data){
					$('.modal-title').text('Brand Updated ...');
					$('#brandModal').modal('show');
					$('#saveButton').hide();
					$('#updateButton').show();

					$('#id').val(data.id);
					$('#name').val(data.name);
					$('#nameError').text('');

				}
		});

	}

	//<===============brand update ==============>

	$(document).on('click', '#updateButton', function(){
			let name = $('#name').val();
			let id = $('#id').val();

			$.ajax({
					type:'post',
					dataType:'json',
					data:{name:name},
					url:'/admin/brand/update/'+id,
					success:function(data){
						toastr.success('Brand Updated Successfull','Success');
						$('.modal-title').text('Brand Created ...');
						$('#brandModal').modal('hide');
						$('#saveButton').show();
						$('#updateButton').hide();
						clearData();
						alldata();
					},
					error:function(error){
					$('#nameError').text(error.responseJSON.errors.name)
				}
			});

	});

		//<===============brand delete ==============>

	function deleteData(id){

		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		  if (result.isConfirmed) {

		  	$.ajax({
				type:'get',
				dataType:'json',
				url:'/admin/brand/destroy/'+id,
				success:function(data){
					// toastr.success('Brand Deleted Successfull','Success');
					alldata();
					Swal.fire(
				      'Deleted!',
				      'Your file has been deleted.',
				      'success'
				    )
				}
			});

		  }
		})

	}

</script>

@endsection
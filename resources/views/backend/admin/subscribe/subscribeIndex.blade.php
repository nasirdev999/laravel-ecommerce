@extends('backend.master')

@section('title')
	Subscribe
@endsection

@section('heading')
	Subscribe Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Subscribe List</h3>
          <button type="button" class="btn btn-success float-right"><i class="fa fa-list"></i> PDF</button>
      	</div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$subscibe)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $subscibe->email }}</td>
        			<td>
        				<a title="Edit" href="" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
@extends('backend.master')

@section('title')
	Banner update 
@endsection

@section('heading')
	Banner Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Banner Updated</h3>
	          <a href="{{ route('admin.banner.index') }}" type="button" class="btn btn-success float-right"><i class="fa fa-list"></i> Banner List</a>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	          	<form action="{{ route('admin.banner.update',$editData->id) }}" method="post" id="myForm" enctype="multipart/form-data">
	          		@csrf
	          		@method('PUT')
	          		<div class="form-row">
	          			<div class="form-group col-md-4">
			              	<label for="name">Heading One </label>
			              	<input type="text" name="heading1" id="heading1" class="form-control" value="{{ $editData->heading1 }}">
			            </div>
			            <div class="form-group col-md-4">
			              	<label for="name">Heading Two </label>
			              	<input type="text" name="heading2" id="heading2" class="form-control" value="{{ $editData->heading2 }}">
			            </div>
			            <div class="form-group col-md-4">
			              	<label for="name">Paragraph One </label>
			              	<input type="text" name="pra1" id="pra1" class="form-control" value="{{ $editData->pra1 }}">
			            </div>
			            <div class="form-group col-md-4">
			              	<label for="name">Paragraph Two </label>
			              	<input type="text" name="pra2" id="pra2" class="form-control" value="{{ $editData->pra2 }}">
			            </div>
			            <div class="form-group col-md-4">
			              	<label for="name">Image </label>
			              	<input type="file" name="image" id="image" class="form-control">
			            </div>
			            <div class="form-group col-md-4">
			            	<img src="{{ asset('upload/banner/'.$editData->image) }}" alt="" height="60px" width="60px">
			            </div>
	          		</div>
          			<div class="form-group col-md-2">
          				<button type="submit" class="btn btn-primary">Update</button>
          			</div>
	          	</form>
	      	</div>
    	</div>
    </div>
</div>

</section>

@endsection
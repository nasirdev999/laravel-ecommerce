@extends('backend.master')

@section('title')
	Banner
@endsection

@section('heading')
	Banner Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Banner List</h3>
          @if(count($alldata) > 0)

          @else
          <button onclick="formShow()" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Add Banner</button>
          @endif
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Heading One</th>
                <th>Heading Two</th>
                <th>Pra1</th>
                <th>Pra2</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$banner)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $banner->heading1 }}</td>
        			<td>{{ $banner->heading2 }}</td>
        			<td>{{ $banner->pra1 }}</td>
        			<td>{{ $banner->pra2 }}</td>
        			<td><img src="{{ asset('upload/banner/'.$banner->image) }}" alt="" height="70px" width="70px"></td>
        			<td>
        				<a title="Edit" href="{{ route('admin.banner.edit',$banner->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
          	<form action="{{ route('admin.banner.store') }}" id="myForm" method="post" enctype="multipart/form-data">
          		@csrf
	            <div class="modal-header">
	              <h4 class="modal-title">Banner Created ...</h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <div class="form-group">
	              	<label for="name">Heading One </label>
	              	<input type="text" name="heading1" id="heading1" class="form-control" placeholder="Heading One">
	              </div>
	              <div class="form-group">
	              	<label for="name">Heading Two </label>
	              	<input type="text" name="heading2" id="heading2" class="form-control" placeholder="Heading Two">
	              </div>
	              <div class="form-group">
	              	<label for="name">Paragraph One </label>
	              	<input type="text" name="pra1" id="pra1" class="form-control" placeholder="Paragraph One">
	              </div>
	              <div class="form-group">
	              	<label for="name">Paragraph Two </label>
	              	<input type="text" name="pra2" id="pra2" class="form-control" placeholder="Paragraph Two">
	              </div>
	              <div class="form-group">
	              	<label for="name">Image </label>
	              	<input type="file" name="image" id="image" class="form-control">
	              </div>
	            </div>
	            <div class="modal-footer justify-content-between">
	              <button id="saveButton" type="submit" class="btn btn-primary">Save</button>
	            </div>
	          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->

</div>
</section>

<script type="text/javascript">
	function formShow(){
		$('#myModal').modal('show');
	}

</script>


@endsection
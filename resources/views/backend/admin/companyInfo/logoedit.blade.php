@extends('backend.master')

@section('title')
	Company logo edit
@endsection

@section('heading')
	Company Information Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Company logo Edit</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
         	<form action="{{ route('admin.company.logo.update',$editData->id) }}" method="post" enctype="multipart/form-data">
         		@csrf
         		<div class="form-group">
         			<label for="">Special Later 1</label>
         			<input type="text" name="special_later1" value="{{ $editData->special_later1 }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Special Later 2</label>
         			<input type="text" name="special_later2" value="{{ $editData->special_later2 }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Word1</label>
         			<input type="text" name="word1" value="{{ $editData->word1 }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Word2</label>
         			<input type="text" name="word2" value="{{ $editData->word2 }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Image</label>
         			<input type="file" name="image" class="form-control">
         			<img src="{{ asset('upload/companyLogo/'.$editData->image) }}" alt="" height="70px" width="70px">
         		</div>
         		<div class="form-group">
         			<button class="btn btn-primary">Submit</button>
         		</div>
         	</form>
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
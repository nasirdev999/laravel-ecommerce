@extends('backend.master')

@section('title')
	Company information edit
@endsection

@section('heading')
	Company Information Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Information Edit</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
         	<form action="{{ route('admin.company.info.update',$editData->id) }}" method="post">
         		@csrf
         		<div class="form-group">
         			<label for="">Email</label>
         			<input type="text" name="email" value="{{ $editData->email }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Phone</label>
         			<input type="text" name="phone" value="{{ $editData->phone }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Fax or Telephone</label>
         			<input type="text" name="fax" value="{{ $editData->fax }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Address</label>
         			<input type="text" name="address" value="{{ $editData->address }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Facebook</label>
         			<input type="text" name="facebook" value="{{ $editData->facebook }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Twitter</label>
         			<input type="text" name="twitter" value="{{ $editData->twitter }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<label for="">Google Plus</label>
         			<input type="text" name="google" value="{{ $editData->gooleplus }}" class="form-control">
         		</div>
         		<div class="form-group">
         			<button class="btn btn-primary">Submit</button>
         		</div>
         	</form>
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
@extends('backend.master')

@section('title')
	Company logo
@endsection

@section('heading')
	Company Logo Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Logo List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Special Later1</th>
                <th>Special Later2</th>
                <th>Word 1</th>
                <th>Word 2</th>
                <th>image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$logo)
        		<tr>
        			<td>{{ $logo->special_later1 }}</td>
        			<td>{{ $logo->special_later2 }}</td>
        			<td>{{ $logo->word1 }}</td>
        			<td>{{ $logo->word2 }}</td>
        			<td><img src="{{ asset('upload/companyLogo/'.$logo->image) }}" alt="" height="70px" width="70px"></td>
        			<td>
        				<a title="Edit | View" href="{{ route('admin.company.logo.edit',$logo->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> | <i class="fa fa-eye"></i></a>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
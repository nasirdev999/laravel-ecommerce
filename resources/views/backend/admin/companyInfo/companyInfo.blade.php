@extends('backend.master')

@section('title')
	Company Information
@endsection

@section('heading')
	Company Information Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Company Information List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Email</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$info)
        		<tr>
        			<td>{{ $info->email }}</td>
        			<td>{{ $info->phone }}</td>
        			<td>{{ $info->address }}</td>
        			<td>
        				<a title="Edit | View" href="{{ route('admin.company.info.edit',$info->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i> | <i class="fa fa-eye"></i></a>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>



@endsection
@extends('backend.master')

@section('title')
	Sub category
@endsection

@section('heading')
	Sub Category Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Sub Category List</h3>
          <button onclick="formShow()" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Add Sub Category</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Slug</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$subcategory)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $subcategory->category->name }}</td>
        			<td>{{ $subcategory->name }}</td>
        			<td>{{ $subcategory->slug }}</td>
        			<td>
        				<a title="Edit" href="{{ route('admin.subcategory.edit',$subcategory->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				<button onclick="deleteFrom({{ $subcategory->id }})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
        				<form id="delete-form-{{ $subcategory->id }}" action="{{ route('admin.subcategory.destroy',$subcategory->id) }}" method="post">
        					@csrf
        					@method('DELETE')
        				</form>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
          	<form action="{{ route('admin.subcategory.store') }}" id="myForm" method="post">
          		@csrf
	            <div class="modal-header">
	              <h4 class="modal-title">Category Created ...</h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              <div class="form-group">
	              	<label for="name">Category Name </label>
	              	<select name="category_id" id="" class="form-control select2bs4" style="width: 100%;height: 38px">
	              		<option value="">Select Category</option>
	              		@foreach($categoris as $category)
	              			<option value="{{ $category->id }}">{{ $category->name }}</option>
	              		@endforeach
	              	</select>
	              </div>
	              <div class="form-group">
	              	<label for="name">Sub Category Name </label>
	              	<input type="text" name="name" id="name" class="form-control" placeholder="Sub Category Name">
	              </div>
	            </div>
	            <div class="modal-footer justify-content-between">
	              <button id="saveButton" type="submit" class="btn btn-primary">Save</button>
	            </div>
	          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->

</div>
</section>

<script type="text/javascript">
	function formShow(){
		$('#myModal').modal('show');
	}

	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      category_id: {
	        required: true,
	        category_id: true,
	      }
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      category_id: {
	        required: "Please enter a categroy name",
	        category_id: "Please enter a vaild categroy name"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>

<script>
	function deleteFrom(id){
		const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        });

        swalWithBootstrapButtons.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            event.preventDefault();
            document.getElementById('delete-form-'+id).submit();
          } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons.fire(
              'Cancelled',
              'Your imaginary file is safe :)',
              'error'
            )
          }
        })

	}
</script>

@endsection
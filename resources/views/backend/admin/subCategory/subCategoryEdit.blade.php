@extends('backend.master')

@section('title')
	Sub Category update 
@endsection

@section('heading')
	Sub Category Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Sub Category Updated</h3>
	          <a href="{{ route('admin.subcategory.index') }}" type="button" class="btn btn-success float-right"><i class="fa fa-list"></i> Sub Category List</a>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	          	<form action="{{ route('admin.subcategory.update',$editData->id) }}" method="post" id="myForm">
	          		@csrf
	          		@method('PUT')
	          		<div class="form-row">
		          		<div class="form-group col-md-4">
			              	<label for="name">Category Name </label>
			              	<select name="category_id" id="" class="form-control select2bs4" style="width: 100%;height: 38px">
			              		<option value="">Select Category</option>
			              		@foreach($categoris as $category)
			              			<option {{ $editData->category_id == $category->id ? 'selected':''}} value="{{ $category->id }}">{{ $category->name }}</option>
			              		@endforeach
			              	</select>
		             	</div>
		              	<div class="form-group col-md-4">
			              	<label for="name">Sub Category Name </label>
			              	<input type="text" name="name" id="name" class="form-control" value="{{ $editData->name }}">
		              	</div>
		            </div>
	              	<div class="form-group col-md-2">
		              	<button type="submit" class="btn btn-primary">Update</button>
	              	</div>
	          	</form>
	      	</div>
    	</div>
    </div>
</div>

<script type="text/javascript">
	function formShow(){
		$('#myModal').modal('show');
	}

	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      category_id: {
	        required: true,
	        category_id: true,
	      }
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      category_id: {
	        required: "Please enter a categroy name",
	        category_id: "Please enter a vaild categroy name"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>

</section>

@endsection
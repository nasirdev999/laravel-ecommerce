<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
</head>
<body>
	<p>To : {{ $toemail }}</p><br>
	<p>From : {{ $fromemail }}</p><br>
	<p>Subject : <b>{{ $subject }}</b></p><br>
	<p>Message : {{ $messages }}</p><br>
</body>
</html>
@extends('backend.master')

@section('title')
 	message details
@endsection

@section('heading')
	Contact Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Unseen Message Details</h3>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	        	<form action="{{ route('admin.contact.update.list',$messageView->id) }}" method="post">
	        		@csrf
	        		<div class="form-group">
	        			<label for="">Name </label>
	        			<input type="text" name="name" value="{{ $messageView->name }}" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">Email Address </label>
	        			<input type="text" name="email" value="{{ $messageView->email }}" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">Subject </label>
	        			<input type="text" name="subject" value="{{ $messageView->subject }}" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">Message </label>
	        			<textarea name="message" id="" cols="30" rows="5" class="form-control">
	        				{{ $messageView->message }}
	        			</textarea>
	        		</div>
	        		<div class="form-group">
	        			<input type="submit" class="btn btn-primary">
	        		</div>
	        	</form>
	      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
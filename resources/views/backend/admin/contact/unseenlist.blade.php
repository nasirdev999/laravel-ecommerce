@extends('backend.master')

@section('title')
	unseen message
@endsection

@section('heading')
	Contact Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Unseen Message List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Message</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($unseenlists as $key=>$message)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $message->name }}</td>
        			<td>{{ $message->email }}</td>
        			<td>{{ $message->subject }}</td>
        			<td>{{ $message->message }}</td>
        			<td>
        				<a title="Seen" href="{{ route('admin.contact.reply',$message->id) }}" class="btn btn-sm btn-info">Reply</a>
        				<a  href="{{ route('admin.contact.unseen.view.list',$message->id) }}" title="Seen" class="btn btn-sm btn-success">Seen</a>
        				
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
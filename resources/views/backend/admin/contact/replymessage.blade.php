@extends('backend.master')

@section('title')
 	message reply
@endsection

@section('heading')
	Contact Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Reply Message Sent</h3>
	        </div>
	        <!-- /.card-header -->
	        <div class="card-body">
	        	<form action="{{ route('admin.contact.reply.send') }}" method="get">
	        		<div class="form-group">
	        			<label for="">To </label>
	        			<input type="text" name="toemail" value="" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">From </label>
	        			<input type="text" name="fromemail" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">Subject </label>
	        			<input type="text" name="subject" class="form-control">
	        		</div>
	        		<div class="form-group">
	        			<label for="">Message </label>
	        			<textarea name="messages" id="" cols="30" rows="5" class="form-control">
	        				
	        			</textarea>
	        		</div>
	        		<div class="form-group">
	        			<input type="submit" class="btn btn-primary">
	        		</div>
	        	</form>
	      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
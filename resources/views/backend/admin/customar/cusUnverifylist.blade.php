cusUnverifylist.blade.php
@extends('backend.master')

@section('title')
	customar verify list
@endsection

@section('heading')
	Customar Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Customar List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Code</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$customar)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $customar->name }}</td>
        			<td>{{ $customar->email }}</td>
        			<td>{{ $customar->phone }}</td>
        			<td>{{ $customar->code }}</td>
        			<td>
        				<img src="{{ asset('upload/customarImage/'.$customar->image) }}" alt="" height="50px" width="50px">
        			</td>
        			<td>
        				<button title="Delete" onclick="deleteFrom({{ $customar->id }})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                        <form id="delete-form-{{ $customar->id }}" action="{{ route('admin.customar.destroy',$customar->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                        </form>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>

<script>
    function deleteFrom(id){
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        });

        swalWithBootstrapButtons.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            event.preventDefault();
            document.getElementById('delete-form-'+id).submit();
          } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
          ) {
            swalWithBootstrapButtons.fire(
              'Cancelled',
              'Your imaginary file is safe :)',
              'error'
            )
          }
        })

    }
</script>

@endsection
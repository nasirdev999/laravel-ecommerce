@extends('backend.master')

@section('title')
	customar verify list
@endsection

@section('heading')
	Customar Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Customar List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Code</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$customar)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $customar->name }}</td>
        			<td>{{ $customar->email }}</td>
        			<td>{{ $customar->phone }}</td>
        			<td>{{ $customar->code }}</td>
        			<td>
        				<img src="{{ asset('upload/customarImage/'.$customar->image) }}" alt="" height="50px" width="50px">
        			</td>
        			<td>
        				<a title="Edit" href="" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
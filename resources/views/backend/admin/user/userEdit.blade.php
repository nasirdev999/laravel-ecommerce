@extends('backend.master')

@section('title')
	User edit
@endsection

@section('heading')
	User Manage
@endsection

@section('mainsection')

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
	        <div class="card-header">
	          <h3 class="card-title">User Edit</h3>
	          <a href="{{ route('admin.user.index') }}" class="btn btn-success float-right"><i class="fa fa-list"></i> User List</a>
	        </div>
        	<!-- /.card-header -->
	        <div class="card-body">
	        	<form action="{{ route('admin.user.update',$editData->id) }}" method="post" name="myForm" id="myForm">
	        		@csrf
	        		<div class="form-row">
	          			<div class="form-group col-md-4">
			              	<label for="name">User Name </label>
			              	<input type="text" name="name" id="name" class="form-control" value="{{ $editData->name }}">
		              	</div>
		              	<div class="form-group col-md-4">
			              	<label for="name">User Email </label>
			              	<input type="text" name="email" id="Email" class="form-control" value="{{ $editData->email }}">
		              	</div>
		              	<div class="form-group col-md-4">
			              	<label for="name">User Type </label>
			              	<select name="role_id" id="" class="form-control">
			              		<option value="">Select A Option</option>
			              		<option value="1">Admin</option>
			              		<option value="2">Author</option>
			              		<option value="3">Editor</option>
			              	</select>
		              	</div>
		              	<div class="form-group col-md-4">
			              	<label for="name">User Password </label>
			              	<input type="password" name="password" id="password" class="form-control" placeholder="Password">
		              	</div>
		              	<div class="form-group col-md-3" style="margin-top:35px;">
		              		<button id="updateButton" type="submit" class="btn btn-primary">Update</button>
		              	</div>
	          		</div>
	        	</form>
	          
	      	</div>
    	</div>
    </div>
</div>
</section>

<script type="text/javascript">
	
	document.forms['myForm'].elements['userType'].value = '{{ $editData->userType }}';

	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      email: {
	        required: true,
	        email: true,
	      },
	      userType: {
	        required: true,
	        userType: true,
	      },
	      password: {
	        required: true,
	        password: true,
	      }
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      email: {
	        required: "Please enter a email",
	        email: "Please enter a vaild email"
	      },
	      userType: {
	        required: "Please enter a userType",
	        userType: "Please enter a vaild userType"
	      },
	      password: {
	        required: "Please enter a password",
	        password: "Please enter a vaild password"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>
@endsection
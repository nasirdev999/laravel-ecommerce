@extends('backend.master')

@section('title')
	User
@endsection

@section('heading')
	User Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">User List</h3>
          <button onclick="formShow()" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Add User</button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>User Type</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$user)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $user->userType }}</td>
        			<td>{{ $user->name }}</td>
        			<td>{{ $user->email }}</td>
        			<td>
        				<a title="Edit" href="{{ route('admin.user.edit',$user->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
          	<form action="{{ route('admin.user.store') }}" id="myForm" method="post">
          		@csrf
	            <div class="modal-header">
	              <h4 class="modal-title">User Created ...</h4>
	              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	              </button>
	            </div>
	            <div class="modal-body">
	              	<div class="form-group">
		              	<label for="name">User Name </label>
		              	<input type="text" name="name" id="name" class="form-control" placeholder="User Name">
	              	</div>
	              	<div class="form-group">
		              	<label for="name">User Email </label>
		              	<input type="text" name="email" id="Email" class="form-control" placeholder="User Email">
	              	</div>
	              	<div class="form-group">
		              	<label for="name">User Type </label>
		              	<select name="role_id" id="" class="form-control">
		              		<option value="">Select A Option</option>
		              		<option value="1">Admin</option>
		              		<option value="2">Author</option>
		              		<option value="3">Editor</option>
		              	</select>
	              	</div>
	              	<div class="form-group">
		              	<label for="name">User Password </label>
		              	<input type="password" name="password" id="password" class="form-control" placeholder="Password">
	              	</div>
	            </div>
	            <div class="modal-footer justify-content-between">
	              <button id="saveButton" type="submit" class="btn btn-primary">Save</button>
	            </div>
	          </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->

</div>
</section>

<script type="text/javascript">
	function formShow(){
		$('#myModal').modal('show');
	}

	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      email: {
	        required: true,
	        email: true,
	      },
	      userType: {
	        required: true,
	        userType: true,
	      },
	      password: {
	        required: true,
	        password: true,
	      },
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      email: {
	        required: "Please enter a email",
	        email: "Please enter a vaild email"
	      },
	      userType: {
	        required: "Please enter a userType",
	        userType: "Please enter a vaild userType"
	      },
	      password: {
	        required: "Please enter a password",
	        password: "Please enter a vaild password"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>


@endsection
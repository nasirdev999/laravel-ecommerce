@extends('backend.master')

@section('title')
	user profile
@endsection

@section('heading')
	User Profile Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">

    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">User Profile View</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        	<form action="{{ route('admin.user.setting.profile.update',$userData->id) }}" method="post" enctype="multipart/form-data">
        		@csrf
    			<div class="form-group">
    				<label for="">Name</label>
    				<input type="text" name="name" class="form-control" value="{{ $userData->name }}">
    			</div>
    			<div class="form-group">
    				<label for="">Email Address</label>
    				<input type="email" name="email" class="form-control" value="{{ $userData->email }}">
    			</div>
    			<div class="form-group">
    				<label for="">Address </label>
    				<input type="text" name="address" class="form-control" value="{{ $userData->address }}">
    			</div>
    			<div class="form-group">
    				<label for="">User Image </label>
    				<input type="file" name="image" class="form-control"><br>
    				<img src="{{ asset('upload/userImage/'.$userData->image) }}" alt="" height="70px" width="70px">
    			</div>
    			<div class="form-group">
    				<label for="">Details </label>
    				<textarea name="details" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          	{{ $userData->details }}
                    </textarea>
    			</div>
    			<div class="form-group">
    				<input type="submit" class="btn btn-primary">
    			</div>
        	</form>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
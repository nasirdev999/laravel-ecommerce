@extends('backend.master')

@section('title')
	user password change
@endsection

@section('heading')
	User Password Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">

    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">User Password View</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        	<form action="{{ route('editor.user.setting.password.update') }}" method="post">
        		@csrf
    			<div class="form-group">
    				<label for="">Old Password</label>
    				<input type="password" name="old_password" class="form-control">
    			</div>
    			<div class="form-group">
    				<label for="">New Password</label>
    				<input type="password" name="password" class="form-control">
    			</div>
    			<div class="form-group">
    				<label for="">Confirm Password </label>
    				<input type="password" name="password_confirmation" class="form-control">
    			</div>
    			
    			<div class="form-group">
    				<input type="submit" class="btn btn-primary">
    			</div>
        	</form>
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
@extends('backend.master')

@section('title')
	order approve invoice
@endsection

@section('heading')
	Order Invoice
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <!-- /.card-header -->
        <div class="card-body">
         	<div class="callout callout-info">
              <h5><i class="fas fa-info"></i> Note: <a href="{{ route('editor.order.pending') }}" class="btn btn-sm btn-primary float-right">Back</a></h5>
              Your are checking,is someone ok.
            </div>


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    Order Invoice
                    <small class="float-right">Date: {{ date('d-m-Y',strtotime($invoiceOrder->order_date)) }}</small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>Customar Information</strong><br>
                    {{ $invoiceOrder->customar->name }}<br>
                    {{ $invoiceOrder->customar->phone }}<br>
                    {{ $invoiceOrder->customar->address }}<br>
                    {{ $invoiceOrder->customar->city }}<br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                 
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice #007612</b><br>
                  <br>
                  <b>Order ID:</b> #or-{{ $invoiceOrder->order_no }}<br>
                  <b>Payment Date:</b> {{ date('d-m-Y',strtotime($invoiceOrder->order_date)) }}<br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Description</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    	@php
                    		$sum = 0;
                    	@endphp
                    	@foreach($invoiceOrder->order_details as $order_detail)
	                    <tr>
	                      <td>{{ $order_detail->product->name }}</td>
	                      <td>{!! html_entity_decode($order_detail->product->short_details) !!}</td>
	                      <td>{{ $order_detail->quantity }}</td>
	                      <td>{{ $order_detail->product->discount_price }}</td>
	                      <td>{{ $order_detail->quantity * $order_detail->product->discount_price }}</td>
	                    </tr>
	                    	@php
	                    		$sum += $order_detail->quantity * $order_detail->product->discount_price;
	                    	@endphp
	                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">
                  <h3>Payment Methods: <br></h3>
                  <b>
                  	@if($invoiceOrder->payment->payment_method == 'cash_payment')
                  		Payment Type : {{ $invoiceOrder->payment->payment_method }}
                  	@else
                  		Payment Type : {{ $invoiceOrder->payment->payment_method }}<br>
                  		Phone : {{ $invoiceOrder->payment->phone }} <br>
                  		Transaction No : {{ $invoiceOrder->payment->transaction_no }} 
                  	@endif
                  </b>
                 	
                </div>
                <!-- /.col -->
                <div class="col-6">
                  <p class="lead">Amount Due 2/22/2014</p>

                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>$ {{ $sum }}</td>
                      </tr>
                      <tr>
                        <th>Tax (7%)</th>
                        <td>$ {{ $tax = ($sum/100)*7 }}</td>
                      </tr>
                      <tr>
                        <th>Shipping: (free)</th>
                        <td>$0.00</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>$ {{ $grandToatal = $sum + $tax }}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <form action="{{ route('editor.order.approval.update',$invoiceOrder->id) }}" method="post">
                  	@csrf
                  	<button type="submit" class="btn btn-success float-right"><i class="fas fa-check"></i> Approved
                  	</button>
                  </form>
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
      	</div>
    	</div>
    </div>

</div>
</section>


@endsection
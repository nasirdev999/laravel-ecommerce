@extends('backend.master')

@section('title')
	Product
@endsection

@section('heading')
	Product Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Product List</h3>
          <a href="{{ route('author.product.create') }}" class="btn btn-primary float-right"><i class="fa fa-plus-circle" ></i> Add Product</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <table id="datatables" class="table table-bordered table-striped product">
            <thead>
              <tr>
                <th>SL.No</th>
                <th>Author</th>
                <th>Name</th>
                <th>Image</th>
                <th>Status</th>
                <th>Is Approve</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            	@foreach($alldata as $key=>$product)
        		<tr>
        			<td>{{ $key + 1 }}</td>
        			<td>{{ $product->user->userType }}</td>
        			<td>{{ $product->name }}</td>
        			<td><img src="{{ asset('upload/productImage/'.$product->image) }}" alt="" height="50px" width="50px"></td>
        			<td>
        				@if($product->status == true)
        					<span class="badge badge-success">Active</span>
        				@else
        					<span class="badge badge-danger">Pending</span>
        				@endif		
        			</td>
                    <td>
                        @if($product->is_approve == true)
                            <span class="badge badge-info">Approved</span>
                        @else
                            <span class="badge badge-warning">Pending</span>
                        @endif      
                    </td>
        			<td>
        				<a title="View" href="{{ route('author.product.show',$product->id) }}" class="btn btn-sm btn-success"><i class="fa fa-eye"></i></a>
        				<a title="Edit" href="{{ route('author.product.edit',$product->id) }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
        				<button title="Delete" onclick="deleteData({{ $product->id }})" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
        				<form action="{{ route('author.product.destroy',$product->id) }}" method="post" id="form-delete-{{ $product->id }}">
        					@csrf
        					@method('DELETE')
        				</form>
        			</td>
        		</tr>
            	@endforeach
          	</tbody>
       	 	</table>
      	</div>
    	</div>
    </div>
</div>
</section>
<script>
	function deleteData(id){
		document.getElementById('form-delete-'+id).submit();
	}

</script>
@endsection
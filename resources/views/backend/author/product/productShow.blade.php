@extends('backend.master')

@section('title')
	Product details
@endsection

@section('heading')
	Product Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Product Details</h3>
          <a href="{{ route('author.product.index') }}" class="btn btn-primary float-right"> Back</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
         	 <!-- Box Comment -->
	        <div class="row">	 
	         	<div class="col-md-8">
		            <div class="card card-widget">
		              	<div class="card-header">
			                <div class="user-block">
			                  <img class="img-circle" src="" alt="User Image">
			                  <span class="username"><a href="">{{ Auth::user()->name }}</a></span>
			                  <span class="description">{{ Auth::user()->created_at }}</span>
			                </div>
		              	</div>
		              	<div class="card-body">
		              		<h3>{{ $showData->title }}</h3>
		              		<table class="table table-bordered">
		              			<tr>
		              				<th>Product Name : </th>
		              				<td>{{ $showData->name }}</td>
		              			</tr>
		              			<tr>
		              				<th>Category Name : </th>
		              				<td>{{ $showData->category->name }}</td>
		              			</tr>
		              			<tr>
		              				<th>Sub Category Name : </th>
		              				<td>{{ $showData->subCategory->name }}</td>
		              			</tr>
		              			<tr>
		              				<th>Brand Name : </th>
		              				<td>{{ $showData->brand->name }}</td>
		              			</tr>
		              			<tr>
		              				<th>Made Country : </th>
		              				<td>{{ $showData->madeCountry->name }}</td>
		              			</tr>
		              			<tr>
		              				<th>Buying Price : </th>
		              				<td>{{ $showData->buying_price }}</td>
		              			</tr>
		              			<tr>
		              				<th>Selling Price : </th>
		              				<td>{{ $showData->selling_price }}</td>
		              			</tr>
		              			<tr>
		              				<th>Discount Percent : </th>
		              				<td>{{ $showData->discount_percent }} %</td>
		              			</tr>
		              			<tr>
		              				<th>Discount Price : </th>
		              				<td>{{ $showData->discount_price }}</td>
		              			</tr>
		              			<tr>
		              				<th>Product Stock : </th>
		              				<td>{{ $showData->stock }}</td>
		              			</tr>
		              		</table><br><br>
		              		<span><b>Short Description : </b><br> {!! html_entity_decode($showData->short_details) !!}</span>
			                <img class="img-fluid pad" src="{{ asset('upload/productImage/'.$showData->image) }}" alt="Photo"><br><br>
			                <span><b>Long Description : </b><br> </span>
			                <p>{!! html_entity_decode($showData->long_details) !!}</p>
		              	</div>
		            </div>
	            </div>

	            <div class="col-md-4">
		            <div class="card card-primary">
		              <div class="card-header">
		                <h3 class="card-title">Colors Name</h3>
		              </div>
		              <!-- /.card-header -->
		              <div class="card-body">
		                @foreach($colors as $color)
		                	<span class="badge badge-primary">{{ $color->color->name }}</span>
		                @endforeach
		              </div>
		            </div>

		             <div class="card card-success">
		              <div class="card-header">
		                <h3 class="card-title">Units Name</h3>
		              </div>
		              <!-- /.card-header -->
		              <div class="card-body">
		                @foreach($units as $unit)
		                	<span class="badge badge-success">{{ $unit->units->name }}</span>
		                @endforeach
		              </div>
		            </div>

		            <div class="card card-danger">
		              <div class="card-header">
		                <h3 class="card-title">Sub Images</h3>
		              </div>
		              <!-- /.card-header -->
		              <div class="card-body">
		                @foreach($sub_images as $sub_image)
		                	<img src="{{ asset('upload/productImage/productSubImage/'.$sub_image->sub_image) }}" alt="" height="70px" width="70px">
		                @endforeach
		              </div>
		            </div>
	          	</div>

	        </div>
      	</div>
    	</div>
    </div>
</div>
</section>


@endsection
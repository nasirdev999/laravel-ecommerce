@extends('backend.master')

@section('title')
	Product edit
@endsection

@section('heading')
	Product Manage
@endsection

@section('mainsection')
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-12">
		<div class="card">
        <div class="card-header">
          <h3 class="card-title">Product Edit</h3>
          <a href="{{ route('author.product.index') }}" class="btn btn-success float-right"><i class="fa fa-list" ></i> List Product</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
         	<form action="{{ route('author.product.update',$editData->id) }}" method="post" id="myForm" enctype="multipart/form-data">
         		@csrf
         		@method('PUT')
         		<div class="form-row">
         			<div class="form-group col-md-3">
         				<label for="">Name</label>
         				<input type="text" name="name" class="form-control" value="{{ $editData->name }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Title</label>
         				<input type="text" name="title" class="form-control" value="{{ $editData->title }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Category</label>
         				<select name="category_id" id="category_id" class="form-control select2bs4">
         					<option value="">Select Category</option>
         					@foreach($categoris as $category)
         						<option {{ $editData->category_id == $category->id ? 'selected':''}} value="{{ $category->id }}">{{ $category->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Sub Category</label>
         				<select name="subCategory_id" id="subCategory_id" class="form-control select2bs4">
         					<option value="">Select Sub Category</option>
         					@foreach($subCategory as $sub_cate)
								<option {{ $editData->SubCategory_id == $sub_cate->id ? 'selected':''}} value="{{ $sub_cate->id }}">{{ $sub_cate->name }}</option>
         					@endforeach
         					
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Brand</label>
         				<select name="brand_id" id="" class="form-control select2bs4">
         					<option value="">Select Brand</option>
         					@foreach($brands as $brand)
         						<option {{ $editData->brand_id == $brand->id ? 'selected':''}} value="{{ $brand->id }}">{{ $brand->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Color </label>
         				<select name="color_id[]" id="" class="form-control select2bs4" multiple="" data-placeholder="Select Color">
         					<option value="">Select Color</option>
         					@foreach($colors as $color)
         						<option
         						@foreach($productColor as $pro_color)
         							{{ $pro_color->color_id == $color->id ? 'selected':''}}
         						@endforeach
         						value="{{ $color->id }}">{{ $color->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Units </label>
         				<select name="units_id[]" id="" class="form-control select2bs4" multiple="" data-placeholder="Select Units">
         					<option value="">Select Units</option>
         					@foreach($units as $unit)
         						<option
         						@foreach($productUnits as $pro_unit)
         							{{ $pro_unit->units_id == $unit->id ? 'selected':''}}
         						@endforeach
         						value="{{ $unit->id }}">{{ $unit->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Country </label>
         				<select name="MadeCountry_id" id="" class="form-control select2bs4">
         					<option value="">Select Country</option>
         					@foreach($countris as $country)
         						<option {{ $editData->MadeCountry_id == $country->id ? 'selected':''}} value="{{ $country->id }}">{{ $country->name }}</option>
         					@endforeach
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Buying Price</label>
         				<input type="number" name="buying_price" class="form-control" value="{{ $editData->buying_price }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Selling Price</label>
         				<input type="number" name="selling_price" class="form-control" value="{{ $editData->selling_price }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Discount Percent</label>
         				<input type="number" name="discount_percent" class="form-control" value="{{ $editData->discount_percent }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Product Stock</label>
         				<input type="number" name="stock" class="form-control" value="{{ $editData->stock }}">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Image </label>
         				<input type="file" name="image">
         				<img src="{{ asset('upload/productImage/'.$editData->image) }}" alt="" height="50px" width="60px">
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Multiple Sub Image </label>
         				<input type="file" name="sub_image[]" multiple="">
         				@foreach($sub_images as $sub_image)
         					<img src="{{ asset('upload/productImage/productSubImage/'.$sub_image->sub_image) }}" alt="" height="50px" width="60px">
         				@endforeach
         			</div>
         			<div class="form-group col-md-3">
         				<label for="">Is Approve </label>
         				<select name="is_approve" id="is_approve" class="form-control">
         					<option value="">Select Approval</option>
         					<option value="0">Pending</option>
         					<option value="1">Approve</option>
         				</select>
         			</div>
         			<div class="form-group col-md-3">
         				<input {{ $editData->status == '1'? 'checked':'' }} type="checkbox" name="status" value="1">	
         				<label for=""> Product Status </label>
         			</div>
         		</div>
         		<div class="form-row">
         			<div class="form-group col-md-6">
         				<label for="">Short Description </label>
         				<textarea name="short_details" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          	{{ $editData->short_details }}
                          </textarea>
         			</div>
         			<div class="form-group col-md-6">
         				<label for="">Long Description </label>
         				<textarea name="long_details" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                          	{{ $editData->long_details }}
                          </textarea>
         			</div>
         		</div>
         		<div class="form-group">
         			<button class="btn btn-primary">Update</button>
         		</div>
         	</form>
      	</div>
    	</div>
    </div>
</div>
</section>

<script>

	document.forms['myForm'].elements['is_approve'].value = '{{ $editData->is_approve }}';

	$(document).on('change','#category_id', function(){
		let category_id = $(this).val();

		$.ajax({
			type:'get',
			dataType:'json',
			data:{category_id:category_id},
			url:'{{ route("author.get.subcategory") }}',
			success:function(data){
				let html = '<option value="">Select Sub Category</option>'
				$.each(data, function(key, value){
					html += '<option value="'+value.id+'">'+value.name+'</option>'
				});
				$('#subCategory_id').html(html);
			}
		});

	});	
</script>

<script type="text/javascript">


	$(document).ready(function () {
	  $.validator.setDefaults({
	    submitHandler: function () {
	      alert( "Form successful submitted!" );
	    }
	  });
	  $('#myForm').validate({
	    rules: {
	      name: {
	        required: true,
	        name: true,
	      },
	      title: {
	        required: true,
	        title: true,
	      },
	      category_id: {
	        required: true,
	        category_id: true,
	      },
	      brand_id: {
	        required: true,
	        brand_id: true,
	      },
	      MadeCountry_id: {
	        required: true,
	        MadeCountry_id: true,
	      },
	      buying_price: {
	        required: true,
	        buying_price: true,
	      },
		  selling_price: {
	        required: true,
	        selling_price: true,
	      },
	      stock: {
	        required: true,
	        stock: true,
	      },
	      image: {
	        required: true,
	        image: true,
	      },
	    },
	    messages: {
	      name: {
	        required: "Please enter a name",
	        name: "Please enter a vaild name"
	      },
	      title: {
	        required: "Please enter a title",
	        title: "Please enter a vaild title"
	      },
	      category_id: {
	        required: "Please enter a category",
	        category_id: "Please enter a vaild category"
	      },
	      brand_id: {
	        required: "Please enter a brand",
	        brand_id: "Please enter a vaild brand"
	      },
	      MadeCountry_id: {
	        required: "Please enter a MadeCountry",
	        MadeCountry_id: "Please enter a vaild MadeCountry"
	      },
	      buying_price: {
	        required: "Please enter a buying price",
	        buying_price: "Please enter a vaild buying price"
	      },
	      selling_price: {
	        selling_price: "Please enter a selling price",
	        selling_price: "Please enter a vaild selling price"
	      },
	      stock: {
	        required: "Please enter a stock",
	        stock: "Please enter a vaild stock"
	      },
	      image: {
	        required: "Please enter a image",
	        image: "Please enter a vaild image"
	      }
	    },
	    errorElement: 'span',
	    errorPlacement: function (error, element) {
	      error.addClass('invalid-feedback');
	      element.closest('.form-group').append(error);
	    },
	    highlight: function (element, errorClass, validClass) {
	      $(element).addClass('is-invalid');
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).removeClass('is-invalid');
	    }
	  });
	});
</script>

@endsection
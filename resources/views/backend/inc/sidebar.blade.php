<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="{{ asset('backend') }}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('upload/userImage/'.Auth::user()->image) }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
        @if(Auth::user()->userType == 'admin')
          <a href="{{ route('admin.user.setting.profile') }}" class="d-block">{{ Auth::user()->name }}</a>
        @elseif(Auth::user()->userType == 'author')
          <a href="{{ route('author.user.setting.profile') }}" class="d-block">{{ Auth::user()->name }}</a>
        @else
          <a href="{{ route('editor.user.setting.profile') }}" class="d-block">{{ Auth::user()->name }}</a>
        @endif
          
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         @if(Auth::user()->userType == 'admin')
          <li class="nav-item">
            <a href="{{ route('admin.dashboard') }}" class="{{ Request::is('admin/dashboard*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>  
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.user.index') }}" class="{{ Request::is('admin/user*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                User Manage
              </p>
            </a>  
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.company.info.index') }}" class="{{ Request::is('admin/company-information*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Company Information
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.about.index') }}" class="{{ Request::is('admin/about*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                About Us
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.company.logo.index') }}" class="{{ Request::is('admin/company-logo*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Company Logo
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.slider.index') }}" class="{{ Request::is('admin/slider*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Slider Manage
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.banner.index') }}" class="{{ Request::is('admin/banner*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Banner Manage
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.subscribe.index') }}" class="{{ Request::is('admin/subscribe*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Subscribe Manage
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.brand.index') }}" class="{{ Request::is('admin/brand*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Manage Brand
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.category.index') }}" class="{{ Request::is('admin/category*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Category
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.subcategory.index') }}" class="{{ Request::is('admin/subcategory*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Sub Category
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.units.index') }}" class="{{ Request::is('admin/units*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Unit
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.color.index') }}" class="{{ Request::is('admin/color*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Color
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ route('admin.made-country.index') }}" class="{{ Request::is('admin/made-country*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Country
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('admin/customar*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Customar
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.customar.index') }}" class="{{ Request::is('admin/customar/list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.customar.unverify.list') }}" class="{{ Request::is('admin/customar/unverify-list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar Unverify</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('admin/product*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.products.index') }}" class="{{ Request::is('admin/product/list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.products.pending.list') }}" class="{{ Request::is('admin/product/pending-list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending List</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('admin/order*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Order
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.order.pending') }}" class="{{ Request::is('admin/order/pending-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending Order</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('admin.order.approve') }}" class="{{ Request::is('admin/order/approve-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Approve Order</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('admin/contact*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Contact
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.contact.seen.list') }}" class="{{ Request::is('admin/contact/message-seen')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Seen Contact</p>
                </a>
              </li>

               <li class="nav-item">
                <a href="{{ route('admin.contact.unseen.list') }}" class="{{ Request::is('admin/contact/message-unseen')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Unseen Contact</p>
                </a>
              </li>

            </ul>
          </li>

          <li class="nav-header"><i class="fa fa-cog"></i> Setting</li>
          <li class="nav-item {{ Request::is('admin/profile/setting')? 'active':'' }}">
            <a href="{{ route('admin.user.setting.profile') }}" class="nav-link">
              <i class="fa fa-user text-danger"></i>
              <p class="text">Profile</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/password/setting')? 'active':'' }}">
            <a href="{{ route('admin.user.setting.password') }}" class="nav-link">
              <i class="fa fa-key text-warning"></i>
              <p>Password Change</p>
            </a>
          </li>
          <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        
          </li>
          @endif


          @if(Auth::user()->userType == 'author')

          <li class="nav-item">
            <a href="{{ route('author.dashboard') }}" class="{{ Request::is('author/dashboard*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>  
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('author/product*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('author.product.index') }}" class="{{ Request::is('author/product*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product List</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('author/order*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Order
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('author.order.pending') }}" class="{{ Request::is('author/order/pending-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending Order</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('author.order.approve') }}" class="{{ Request::is('author/order/approve-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Approve Order</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('author/customar*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Customar
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('author.customar.index') }}" class="{{ Request::is('author/customar/list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('author.customar.unverify.list') }}" class="{{ Request::is('author/customar/unverify-list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar Unverify</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-header"><i class="fa fa-cog"></i> Setting</li>
          <li class="nav-item {{ Request::is('author/profile/setting')? 'active':'' }}">
            <a href="{{ route('author.user.setting.profile') }}" class="nav-link">
              <i class="fa fa-user text-danger"></i>
              <p class="text">Profile</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('author/password/setting')? 'active':'' }}">
            <a href="{{ route('author.user.setting.password') }}" class="nav-link">
              <i class="fa fa-key text-warning"></i>
              <p>Password Change</p>
            </a>
          </li>
          <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        
          </li>

          @endif

          @if(Auth::user()->userType == 'editor')

          <li class="nav-item">
            <a href="{{ route('editor.dashboard') }}" class="{{ Request::is('editor/dashboard*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>  
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('editor/order*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Order
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('editor.order.pending') }}" class="{{ Request::is('editor/order/pending-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending Order</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="{{ route('editor.order.approve') }}" class="{{ Request::is('editor/order/approve-list*')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Approve Order</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="{{ Request::is('editor/customar*')? 'active':'' }} nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Manage Customar
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('editor.customar.index') }}" class="{{ Request::is('editor/customar/list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('editor.customar.unverify.list') }}" class="{{ Request::is('editor/customar/unverify-list')? 'active':'' }} nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customar Unverify</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-header"><i class="fa fa-cog"></i> Setting</li>
          <li class="nav-item {{ Request::is('editor/profile/setting')? 'active':'' }}">
            <a href="{{ route('editor.user.setting.profile') }}" class="nav-link">
              <i class="fa fa-user text-danger"></i>
              <p class="text">Profile</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('editor/password/setting')? 'active':'' }}">
            <a href="{{ route('editor.user.setting.password') }}" class="nav-link">
              <i class="fa fa-key text-warning"></i>
              <p>Password Change</p>
            </a>
          </li>
          <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i> Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        
          </li>

          @endif


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
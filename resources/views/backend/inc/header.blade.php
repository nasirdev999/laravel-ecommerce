<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>


    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
     
      <!-- Notifications Dropdown Menu -->
      <li>
        @if(Auth::user()->userType == 'admin')
        <a href="{{ route('admin.user.setting.profile') }}" class="dropdown-item {{ Request::is('admin/profile/setting')? 'active':'' }}">
          <i class="far fa-user"></i> Profile
        </a>
        @elseif(Auth::user()->userType == 'author')
        <a href="{{ route('author.user.setting.profile') }}" class="dropdown-item {{ Request::is('author/profile/setting')? 'active':'' }}">
          <i class="far fa-user"></i> Profile
        </a>
        @else
        <a href="{{ route('editor.user.setting.profile') }}" class="dropdown-item {{ Request::is('editor/profile/setting')? 'active':'' }}">
          <i class="far fa-user"></i> Profile
        </a>
        @endif
      </li>
      <li>
          <a class="dropdown-item" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
      </li>
    </ul>
  </nav>
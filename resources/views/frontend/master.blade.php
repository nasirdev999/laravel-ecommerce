<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>@yield('title') | Grocery Shoppy an Ecommerce Category Website</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Grocery Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<!-- <script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script> -->
	<!--//tags -->
	<link href="{{ asset('frontend') }}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontend') }}/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('frontend') }}/css/font-awesome.css" rel="stylesheet">
	<!--pop-up-box-->
	<link href="{{ asset('frontend') }}/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<!--//pop-up-box-->
	<!-- price range -->
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/css/jquery-ui1.css">
	<!-- fonts -->
	<!-- flexslider -->
	<link rel="stylesheet" href="{{ asset('frontend') }}/css/flexslider.css" type="text/css" media="screen" />
	<!-- toastr css -->
	<link rel="stylesheet" href="{{ asset('frontend') }}/toastr/toastr.min.css">
	
	<style>
		.quantitys input{
			width:100px;
			height:30px;
		}
		.quantitys button{
			margin-top: -5px;
		}

		.customar_right{
			box-shadow: 0px 0px 15px 0px #D6D6D6;
		    padding: 30px 20px;
		    margin-bottom: 14px;
			
		}
		.customar_left{
			background: #F9F9F9;
		}
		.customar_left ul{

		}
		.customar_left ul li{
			list-style: none;
		    border-bottom: 2px solid #e4dfdf;
		    padding: 10px;
		}
		.customar_left ul li a{
		    text-decoration: none;
		    color: #aba7a7;
		    padding: 5px;
		    border-radius: 5px;
		    font-size: 17px;
		}
		.customar_left ul li a:hover{
			background: #1accfd;
			color: #fff;
		}

		#cusActive{
			background: #1accfd;
			color: #fff;
		}
	</style>
	<!-- jquery -->
	<script src="{{ asset('frontend') }}/js/jquery-2.1.4.min.js"></script>
	<!-- //jquery -->

	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
</head>

<body>
	@include('frontend.inc.header')
	<!-- //navigation -->


	@yield('mainsection')
	<!-- special offers -->
	
	<!-- //special offers -->
	
	@include('frontend.inc.footer')

	<!-- js-files -->
	

	<!-- popup modal (for signin & signup)-->
	<script src="{{ asset('frontend') }}/js/jquery.magnific-popup.js"></script>
	<script>
		$(document).ready(function () {
			$('.popup-with-zoom-anim').magnificPopup({
				type: 'inline',
				fixedContentPos: false,
				fixedBgPos: true,
				overflowY: 'auto',
				closeBtnInside: true,
				preloader: false,
				midClick: true,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in'
			});

		});
	</script>
	<!-- Large modal -->
	<!-- <script>
		$('#').modal('show');
	</script> -->
	<!-- //popup modal (for signin & signup)-->

	

	<!-- price range (top products) -->
	<script src="{{ asset('frontend') }}/js/jquery-ui.js"></script>
	<script>
		//<![CDATA[ 
		$(window).load(function () {
			$("#slider-range").slider({
				range: true,
				min: 0,
				max: 9000,
				values: [50, 6000],
				slide: function (event, ui) {
					$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
				}
			});
			$("#amount").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

		}); //]]>
	</script>
	<!-- //price range (top products) -->

	<!-- flexisel (for special offers) -->
	<script src="{{ asset('frontend') }}/js/jquery.flexisel.js"></script>
	<script>
		$(window).load(function () {
			$("#flexiselDemo1").flexisel({
				visibleItems: 3,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: {
					portrait: {
						changePoint: 480,
						visibleItems: 1
					},
					landscape: {
						changePoint: 640,
						visibleItems: 2
					},
					tablet: {
						changePoint: 768,
						visibleItems: 2
					}
				}
			});

		});
	</script>
	<!-- //flexisel (for special offers) -->

	<!-- password-script -->
	<script>
		window.onload = function () {
			document.getElementById("password1").onchange = validatePassword;
			document.getElementById("password2").onchange = validatePassword;
		}

		function validatePassword() {
			var pass2 = document.getElementById("password2").value;
			var pass1 = document.getElementById("password1").value;
			if (pass1 != pass2)
				document.getElementById("password2").setCustomValidity("Passwords Don't Match");
			else
				document.getElementById("password2").setCustomValidity('');
			//empty string means no validation error
		}
	</script>
	<!-- //password-script -->

	<!-- smoothscroll -->
	<script src="{{ asset('frontend') }}/js/SmoothScroll.min.js"></script>
	<!-- //smoothscroll -->

	<!-- start-smooth-scrolling -->
	<script src="{{ asset('frontend') }}/js/move-top.js"></script>
	<script src="{{ asset('frontend') }}/js/easing.js"></script>
	<script>
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();

				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->

	<!-- for bootstrap working -->
	<script src="{{ asset('frontend') }}/js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- //js-files -->
	<!-- FlexSlider -->
	<script src="{{ asset('frontend') }}/js/jquery.flexslider.js"></script>
	<script>
		// Can also be used with $(document).ready()
		$(window).load(function () {
			$('.flexslider').flexslider({
				animation: "slide",
				controlNav: "thumbnails"
			});
		});
	</script>
	<!-- //FlexSlider-->

		<!-- easy-responsive-tabs -->
	<link rel="stylesheet" type="text/css" href="{{ asset('frontend') }}/css/easy-responsive-tabs.css " />
	<link rel="stylesheet" href="{{ asset('frontend') }}/css/creditly.css" type="text/css" media="all" />
	<script src="{{ asset('frontend') }}/js/easyResponsiveTabs.js"></script>

	<script>
		$(document).ready(function () {
			//Horizontal Tab
			$('#parentHorizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				tabidentify: 'hor_1', // The tab groups identifier
				activate: function (event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#nested-tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
	</script>
	<!-- //easy-responsive-tabs -->
	<!-- script for tabs -->
	<script>
		$(function () {

			var menu_ul = $('.faq > li > ul'),
				menu_a = $('.faq > li > a');

			menu_ul.hide();

			menu_a.click(function (e) {
				e.preventDefault();
				if (!$(this).hasClass('active')) {
					menu_a.removeClass('active');
					menu_ul.filter(':visible').slideUp('normal');
					$(this).addClass('active').next().stop(true, true).slideDown('normal');
				} else {
					$(this).removeClass('active');
					$(this).next().stop(true, true).slideUp('normal');
				}
			});

		});
	</script>
	<!-- script for tabs -->

	<!-- imagezoom -->
	<script src="{{ asset('frontend') }}/js/imagezoom.js"></script>
	<!-- //imagezoom -->
	<!-- toastr js -->
	<script src="{{ asset('frontend') }}/toastr/toastr.min.js"></script>
	{!! Toastr::message() !!}

	<script>
	    @if($errors->any())
	      @foreach($errors->all() as $error)
	        toastr.error('{{ $error }}','Error',{
	            progressBar:true,
	            closeButton:true,
	        });
	      @endforeach
	    @endif
	</script>
</body>

</html>
@extends('frontend.master')

@section('title')
	product details 
@endsection
@section('mainsection')

<!-- banner-2 -->
	<div class="page-head_agile_info_w3l">

	</div>
	<!-- //banner-2 -->
	<!-- page -->
	<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="{{ route('home') }}">Home</a>
						<i>|</i>
					</li>
					<li>Product Details</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->
	<!-- Single Page -->
	<div class="banner-bootom-w3-agileits">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Product Details
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<div class="col-md-5 single-right-left ">
				<div class="grid images_3_of_2">
					<div class="flexslider">
						<ul class="slides">
							@foreach($pro_subImage as $subImage)
							<li data-thumb="{{ asset('upload/productImage/productSubImage/'.$subImage->sub_image) }}"  height="50px" width="50px">
								<div class="thumb-image">
									<img src="{{ asset('upload/productImage/productSubImage/'.$subImage->sub_image) }}" data-imagezoom="true" class="img-responsive" alt="">
								</div>
							</li>
							@endforeach
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="col-md-7 single-right-left simpleCart_shelfItem">
				<h3>{{ $productDetail->title }}</h3>
				<p>
					<span class="item_price">$ {{ $productDetail->discount_price }}</span>
					<del>$ {{ $productDetail->selling_price }}</del>
					<label>Free delivery</label>
				</p>
				<div class="single-infoagile">
					<ul>
						<li>
							Cash on Delivery Eligible.
						</li>
						<li>
							Shipping Speed to Delivery.
						</li>
						<li>
							<strong>Category</strong> : {{ $productDetail->category->name }}
						</li>
						<li>
							<strong>Sub Category</strong> : {{ $productDetail->subCategory->name }}
						</li>
						<li>
							<strong>Brand</strong> : {{ $productDetail->brand->name }}
						</li>
						<li>
							Made in <strong>{{ $productDetail->madeCountry->name }}</strong>
						</li>
					</ul>
				</div>
				<div class="product-single-w3l">
					<p>
						<i class="fa fa-hand-o-right" aria-hidden="true"></i>Product 
						<label>Short</label> Description.</p>
					<ul>
						{!! html_entity_decode($productDetail->short_details)!!}
					</ul>
					<p>
						<i class="fa fa-refresh" aria-hidden="true"></i>All products are
						<label>non-returnable.</label>
					</p>
				</div>

				<div class="occasion-cart">
					<?php
						$cusID = Session::get('cusID');
					?>
					@if($cusID != null)
					<form action="{{ route('checkout.store',$productDetail->slug) }}" method="post">
						@csrf
						<b>Quantity : </b><input type="number" name="quantity" value="1" class="form-control" min="1" style="width: 38%;margin-bottom: 10px;">
						<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
							<input type="submit" name="submit" value="Add to cart" class="button" />
						</div>
					</form>
					@else
						<b>Quantity : </b><input type="number" name="quantity" value="1" class="form-control" min="1" style="width: 38%;margin-bottom: 10px;">
						<div data-toggle="modal" data-target="#myModal1" onclick="(toastr.info('Please Login Your Account','Information'))" class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
							<input type="submit" name="submit" value="Add to cart" class="button" />
						</div>
					@endif

				</div>

			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //Single Page -->
	<!-- special offers -->
	<div class="featured-section" id="projects">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Add More
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<div class="content-bottom-in">
				<ul id="flexiselDemo1">
					@foreach($add_more_products as $add_more_product)
					<li>
						<div class="w3l-specilamk">
							<div class="speioffer-agile">
								<a href="{{ route('product.details',$add_more_product->slug) }}">
									<img src="{{ asset('upload/productImage/'.$add_more_product->image) }}" alt="" height="100px" width="110px">
								</a>
							</div>
							<div class="product-name-w3l">
								<h4>
									<a href="{{ route('product.details',$add_more_product->slug) }}">{{ $add_more_product->name }}</a>
								</h4>
								<div class="w3l-pricehkj">
									<h6>$ {{ $add_more_product->discount_price }}</h6>
									<p>Save {{ $add_more_product->discount_percent }} %</p>
								</div>
								<?php
									$cusID = Session::get('cusID');
								?>
								@if($cusID != null)
								<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
									<form action="{{ route('checkout.store',$add_more_product->slug) }}" method="post">
										@csrf
										<fieldset>
											<input type="submit" name="submit" value="Add to cart" class="button" />
										</fieldset>
									</form>
								</div>
								@else
								<div data-toggle="modal" data-target="#myModal1" onclick="(toastr.info('Please Login Your Account','Information'))" class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
									<fieldset>
										<input type="submit" name="submit" value="Add to cart" class="button" />
									</fieldset>
								</div>
								@endif
							</div>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<!-- //special offers -->

@endsection
@extends('frontend.master')

@section('title')
	checkout 
@endsection
@section('mainsection')
	
<!-- banner-2 -->
<div class="page-head_agile_info_w3l">

</div>
<!-- //banner-2 -->
<!-- page -->
<div class="services-breadcrumb">
	<div class="agile_inner_breadcrumb">
		<div class="container">
			<ul class="w3_short">
				<li>
					<a href="{{ route('home') }}">Home</a>
					<i>|</i>
				</li>
				<li>Checkout</li>
			</ul>
		</div>
	</div>
</div>
<!-- //page -->
<!-- checkout page -->
<div class="privacy">
	<div class="container">
		<!-- tittle heading -->
		<h3 class="tittle-w3l">Checkout
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<!-- //tittle heading -->
		<div class="checkout-right">
			<h4>Your shopping cart contains:
				<span> Products</span>
			</h4>
			<div class="table-responsive">
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>SL No.</th>
							<th>Product</th>
							<th>Product Name</th>
							<th>Quality</th>
							<th>Price</th>
							<th>Amount</th>
							<th>Remove</th>
						</tr>
					</thead>
					<tbody>
						@php
							$qty = 0;
							$sum = 0;
						@endphp
						@foreach($alldata as $key=>$checkout)
						<tr class="rem1">
							<td class="invert">{{ $key+1 }}</td>
							<td class="invert-image">
								<img src="{{ asset('upload/productImage/'.$checkout->image) }}" alt="" height="40px" >
							</td>
							<td class="invert">{{ $checkout->product_name }}</td>
							<td class="invert">
								<div class="quantitys">
									<form action="{{ route('checkout.update',$checkout->id) }}" method="post">
										@csrf
										<input type="number" name="quantity" min="1" value="{{ $checkout->quantity }}">
										<button class="btn btn-sm btn-info">Update</button>
									</form>
								</div>
							</td>
							<td class="invert">$ {{ $checkout->price }}</td>
							<td class="invert">$ {{ $checkout->amount }}</td>
							<td class="invert">
								<div class="rem">
									<input type="hidden" id="id" value="{{ $checkout->id }}">
									<button class="close1" id="deleteCheckout" style="border: none;"></button>
								</div>
							</td>
						</tr>

							<?php
								$qty += $checkout->quantity;
								$sum += $checkout->amount;

								Session::put('qty', $qty);
								Session::put('total', $sum);

							?>

						@endforeach
					</tbody>
				</table>
				<div class="table-responsive col-md-4 pull-right">
				<table class="timetable_sub">
					<tr>
						<td>Sub Total  </td>
						<td> $ {{ $sum }}</td>
					</tr>
					<tr>
						<td>Shipping <b>(free)</b> </td>
						<td>$ 0.00</td>
					</tr>
					<tr>
						<td>Tax <b>7%</b> </td>
						<td>
							<?php
								echo $tax = ($sum / 100) * 7;
							?>
						</td>
					</tr>
					<tr>
						<td>Grand Total  </td>
						<td>$ 
							@php
								echo $grandTotal = $tax + $sum;
								Session::put('grandTotal', $grandTotal);
							@endphp	
						</td>
					</tr>
				</table>
			</div>
			</div>

		</div>
		<div class="checkout-left">
			<div class="address_form_agile">
				<h4>Add new Details For Delivery</h4>
				<form action="{{ route('shipping.store') }}" method="post" class="creditly-card-form agileinfo_form">
					@csrf
					<div class="creditly-wrapper wthree, w3_agileits_wrapper">
						<div class="information-wrapper">
							<div class="first-row">
								<div class="controls">
									<input class="billing-address-name" type="text" name="name" placeholder="Full Name" required="">
								</div>
								<div class="w3_agileits_card_number_grids">
									<div class="w3_agileits_card_number_grid_left">
										<div class="controls">
											<input type="text" placeholder="Mobile Number" name="phone" required="">
										</div>
									</div>
									<div class="clear"> </div>
								</div>
								<div class="controls">
									<input type="text" placeholder="Town/City" name="city" required="">
								</div>
								<div class="controls">
									<input type="text" placeholder="Full Address" name="address" required="">
								</div>
							</div>
							<button class="submit check_out">Delivery to this Address</button>
						</div>
					</div>
				</form><!-- 
				<div class="checkout-right-basket">
					<a href="payment.html">Make a Payment
						<span class="fa fa-hand-o-right" aria-hidden="true"></span>
					</a>
				</div> -->
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //checkout page -->

<script>

	$(document).on('click','#deleteCheckout', function(){
		let id = $('#id').val();
		$.ajax({
			type:'get',
			dataType:'json',
			url:'/checkout-delete/'+id,
			success:function(data){
				toastr.success('Product Deleted From Checkout','Success');
			}
		});

	});

</script>

@endsection
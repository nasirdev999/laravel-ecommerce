@extends('frontend.master')

@section('title')
	brand | {{ $brand_wise_data->slug }}
@endsection
@section('mainsection')

	<!-- top Products -->
	<div class="ads-grid">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Brand Products
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<!-- product left -->
			@include('frontend.inc.sidebar')
			<!-- //product left -->
			<!-- product right -->
			<div class="agileinfo-ads-display col-md-9">
				<div class="wrapper">
					<!-- third section (oils) -->
					<div class="product-sec1">
						@foreach($brandProduct as $product)
							<div class="col-md-4 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">
										<img src="{{ asset('upload/productImage/'.$product->image)}}" alt="" height="120px" width="100px">
										<div class="men-cart-pro">
											<div class="inner-men-cart-pro">
												<a href="{{ route('product.details',$product->slug) }}" class="link-product-add-cart">Quick View</a>
											</div>
										</div>
										<span class="product-new-top">New</span>
									</div>
									<div class="item-info-product ">
										<h4>
											<a href="{{ route('product.details',$product->slug) }}">{{ $product->name }}</a>
										</h4>
										<div class="info-product-price">
											<span class="item_price">${{ $product->discount_price }}</span>
											<del>${{ $product->selling_price }}</del>
										</div>
										<?php
											$cusID = Session::get('cusID');
										?>
										@if($cusID != null)
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
											<form action="{{ route('checkout.store',$product->slug) }}" method="post">
												@csrf
												<fieldset>
													<input type="submit" name="submit" value="Add to cart" class="button" />
												</fieldset>
											</form>
										</div>
										@else
										<div data-toggle="modal" data-target="#myModal1" onclick="(toastr.info('Please Login Your Account','Information'))" class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
											<fieldset>
												<input type="submit" name="submit" value="Add to cart" class="button" />
											</fieldset>
										</div>
										@endif

									</div>
								</div>
							</div>
						@endforeach

						<div class="clearfix"></div>
					</div>
					{{ $brandProduct->links() }}
				</div>
			</div>
			<!-- //product right -->
		</div>
	</div>
	<!-- //top products -->
	@include('frontend.inc.special_offer')
@endsection
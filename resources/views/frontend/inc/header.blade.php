	<!-- header section -->
	<!-- top-header -->
	<div class="header-most-top">
		<p>Smart Bazer Offer Zone Top Deals & Discounts</p>
	</div>
	<!-- //top-header -->
	<!-- header-bot-->
	<div class="header-bot">
		<div class="header-bot_inner_wthreeinfo_header_mid">
			<!-- header-bot-->
			<div class="col-md-4 logo_agile">
				<h1>
					<a href="{{ route('home') }}">
						<span>{{ $companyLogo->special_later1 }}</span>{{ $companyLogo->word1 }}
						<span>{{ $companyLogo->special_later2 }}</span>{{ $companyLogo->word2 }} <br>
						<img src="{{ asset('upload/companyLogo/'.$companyLogo->image) }}" alt="">
					</a>
				</h1>
			</div>
			<!-- header-bot -->
			<div class="col-md-8 header">
				<!-- header lists -->
				<?php $cusID =  Session::get('cusID');?>
				<ul>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal1">
							<span class="fa fa-truck" aria-hidden="true"></span>Track Order</a>
					</li>
					<li>
						<span class="fa fa-phone" aria-hidden="true"></span> {{ $companyInfo->phone }}
					</li>
					<li>
							@if($cusID == null)
							<a href="#" data-toggle="modal" data-target="#myModal1">
							<span class="fa fa-unlock-alt" aria-hidden="true"></span> Sign In </a>
							@else
							<a href="{{ route('customar.logout') }}">
							<span class="fa fa-lock" aria-hidden="true"></span> Logout </a>
							@endif

					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal2">
							<span class="fa fa-pencil-square-o" aria-hidden="true"></span> Sign Up </a>
					</li>
				</ul>
				<!-- //header lists -->
				<!-- search -->
				<div class="agileits_search">
					<form action="{{ route('search.index') }}" method="get">
						<input name="search" type="search" placeholder="How can we help you today?" value="{{ isset($query)? $query:'' }}">
						<button type="submit" class="btn btn-default" aria-label="Left Align">
							<span class="fa fa-search" aria-hidden="true"> </span>
						</button>
					</form>
				</div>
				<!-- //search -->
				<!-- cart details -->
				<div class="top_nav_right">
					<div class="wthreecartaits wthreecartaits2 cart cart box_1">
						<?php
							$cusID = Session::get('cusID');
						?>
						@if($cusID == null)
							<button class="w3view-cart" type="submit" name="submit" value="">
							<a style="color:#fff;" href="#" data-toggle="modal" data-target="#myModal1"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>
						</button>
						@else
						<button class="w3view-cart" type="submit" name="submit" value="">
							<a href="{{ route('checkout.index') }}" style="color:#fff;"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>
						</button>
						@endif
					</div>
				</div>
				<!-- //cart details -->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- shop locator (popup) -->

	<!-- //shop locator (popup) -->
	<!-- signin Model -->
	<!-- Modal1 --> 
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body modal-body-sub_agile">
					<div class="main-mailposi">
						<span class="fa fa-envelope-o" aria-hidden="true"></span>
					</div>
					<div class="modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign In </h3>
						<p>
							Sign In now, Let's start your Smart Bazer. Don't have an account?
							<a href="#" data-toggle="modal" data-target="#myModal2">
								Sign Up Now</a>
						</p>
						<form action="{{ route('customar.cuslogin') }}" method="get">
							<div class="styled-input agile-styled-input-top">
								<input type="text" placeholder="Email Address" name="email" required="">
							</div>
							<div class="styled-input">
								<input type="password" placeholder="Password" name="password" required="">
							</div>
							<input type="submit" value="Sign In">
						</form>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- //Modal content-->
		</div>
	</div>
	<!-- //Modal1 -->
	<!-- //signin Model -->
	<!-- signup Model -->
	<!-- Modal2 -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body modal-body-sub_agile">
					<div class="main-mailposi">
						<span class="fa fa-envelope-o" aria-hidden="true"></span>
					</div>
					<div class="modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign Up</h3>
						<p>
							Come join the Smart Bazer! Let's set up your Account.
						</p>
						<form action="{{ route('customar.store') }}" method="post">
							@csrf
							<div class="styled-input agile-styled-input-top">
								<input type="text" placeholder="Name" name="name" required="">
							</div>
							<div class="styled-input">
								<input type="email" placeholder="E-mail" name="email" required="">
							</div>
							<div class="styled-input">
								<input type="password" placeholder="Password" name="password" id="password1" required="">
							</div>
							<div class="styled-input">
								<input type="password" placeholder="Confirm Password" name="password_confirmation" id="password2" required="">
							</div>
							<input type="submit" value="Sign Up">
						</form>
						<p>
							<a href="#">By clicking register, I agree to your terms</a>
						</p>
					</div>
				</div>
			</div>
			<!-- //Modal content-->
		</div>
	</div>
	<!-- //Modal2 -->
	<!-- //signup Model -->
	<!-- //header-bot -->
	<!-- navigation -->
	<div class="ban-top">
		<div class="container">
			<div class="agileits-navi_search">
				<form action="#" method="post">
					<select id="agileinfo-nav_search" name="agileinfo_search" required="">
						<option value="">All Categories</option>
						@foreach($categoris as $category)
							<option value="{{ $category->slug }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</form>
			</div>
			<div class="top_nav_left">
				<nav class="navbar navbar-default">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
							    aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav menu__list">
								<li class="{{ Request::is('/')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('home') }}">Home
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="{{ Request::is('all-products/product')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('product.allproducts.data') }}">All Products</a>
								</li>
								<li class="{{ Request::is('abouts')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('about.page') }}">About Us</a>
								</li>
								<li class="{{ Request::is('faqs')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('faqs.page') }}">Faqs</a>
								</li>
								<li class="{{ Request::is('contact')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('contact.index') }}">Contact</a>
								</li>
								@php
									$cusID = Session::get('cusID');
								@endphp
								@if($cusID != null)
								<li class="{{ Request::is('customar/dashboard')?'active':'' }}">
									<a class="nav-stylehead" href="{{ route('customar.cusdashboard') }}">Profile</a>
								</li>
								@endif
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>

<!-- slider section -->

	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators-->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1" class=""></li>
			<li data-target="#myCarousel" data-slide-to="2" class=""></li>
			<li data-target="#myCarousel" data-slide-to="3" class=""></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			@foreach($sliders as $key=>$slider)
			@if($key == 1)
			<div class="item active" style="background:url({{asset('upload/sliderImage/'.$slider->image)}}); background-size:cover;">
			@else
			<div class="item" style="background:url({{asset('upload/sliderImage/'.$slider->image)}}); background-size:cover;" >
			@endif	
				<div class="container">
					<div class="carousel-caption">
						<h3>{{ $slider->heading1 }}
							<span>{{ $slider->heading2 }}</span>
						</h3>
						<p>{{ $slider->para1 }}
							<span>{{ $slider->discount }}</span> {{ $slider->para2 }}</p>
						<a class="button2" href="{{ route('product.allproducts.data') }}">Shop Now </a>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<!-- //banner -->

	
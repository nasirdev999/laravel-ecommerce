<!-- special offers -->
	<div class="featured-section" id="projects">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Special Offers
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<div class="content-bottom-in">
				<ul id="flexiselDemo1">
					@foreach($special_offers as $key=>$special_offer)
					<li>
						<div class="w3l-specilamk">
							<div class="speioffer-agile">
								<a href="{{ route('product.details',$special_offer->slug) }}">
									<img src="{{ asset('upload/productImage/'.$special_offer->image)}}" alt="" height="150px" width="150px">
								</a>
							</div>
							<div class="product-name-w3l">
								<h4>
									<a href="{{ route('product.details',$special_offer->slug) }}">{{ $special_offer->name }}</a>
								</h4>
								<div class="w3l-pricehkj">
									<h6>${{ $special_offer->discount_price }}</h6>
									<p>Save {{ $special_offer->discount_percent }} %</p>
								</div>
								<?php
									$cusID = Session::get('cusID');
								?>
								@if($cusID != null)
								<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
									<form action="{{ route('checkout.store',$special_offer->slug) }}" method="post">
										@csrf
										<fieldset>
											<input type="submit" name="submit" value="Add to cart" class="button" />
										</fieldset>
									</form>
								</div>
								@else
								<div data-toggle="modal" data-target="#myModal1" onclick="(toastr.info('Please Login Your Account','Information'))" class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
									<fieldset>
										<input type="submit" name="submit" value="Add to cart" class="button" />
									</fieldset>
								</div>
								@endif
							</div>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
	<!-- //special offers -->
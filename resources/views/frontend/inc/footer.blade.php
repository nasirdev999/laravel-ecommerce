<!-- newsletter -->
	<div class="footer-top">
		<div class="container-fluid">
			<div class="col-xs-8 agile-leftmk">
				<h2>Get your Samart Bazer delivered from local stores</h2>
				<p>Free Delivery on your first order!</p>
				<form action="{{ route('subscribe.store') }}" method="post">
					@csrf
					<input type="email" placeholder="E-mail" name="email" required="">
					<input type="submit" value="Subscribe">
				</form>
				<div class="newsform-w3l">
					<span class="fa fa-envelope-o" aria-hidden="true"></span>
				</div>
			</div>
			<div class="col-xs-4 w3l-rightmk">
				<img src="{{ asset('frontend')}}/images/tab3.png" alt=" ">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //newsletter -->
	<!-- footer -->
	<footer>
		<div class="container">
			<!-- footer first section -->
			<p class="footer-main">
				<span>"Smart Bazer"</span> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
				magni dolores eos qui ratione voluptatem sequi nesciunt.Sed ut perspiciatis unde omnis iste natus error sit voluptatem
				accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
				beatae vitae dicta sunt explicabo.</p>
			<!-- //footer first section -->
			<!-- footer second section -->
			<div class="w3l-grids-footer">
				<div class="col-xs-4 offer-footer">
					<div class="col-xs-4 icon-fot">
						<span class="fa fa-map-marker" aria-hidden="true"></span>
					</div>
					<div class="col-xs-8 text-form-footer">
						<h3>Track Your Order</h3>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-xs-4 offer-footer">
					<div class="col-xs-4 icon-fot">
						<span class="fa fa-refresh" aria-hidden="true"></span>
					</div>
					<div class="col-xs-8 text-form-footer">
						<h3>Free & Easy Returns</h3>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-xs-4 offer-footer">
					<div class="col-xs-4 icon-fot">
						<span class="fa fa-times" aria-hidden="true"></span>
					</div>
					<div class="col-xs-8 text-form-footer">
						<h3>Online cancellation </h3>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- //footer second section -->
			<!-- footer third section -->
			<div class="footer-info w3-agileits-info">
				<!-- footer categories -->
				<div class="col-sm-5 address-right">
					<div class="col-xs-6 footer-grids">
						<h3>Categories</h3>
						<ul>
							@foreach($categoris as $category)
							<li>
								<a href="{{ route('product.categorywise.data',$category->slug) }}">{{ $category->name }}</a>
							</li>
							@endforeach
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- //footer categories -->
				<!-- quick links -->
				<div class="col-sm-5 address-right">
					<div class="col-xs-6 footer-grids">
						<h3>Quick Links</h3>
						<ul>
							<li>
								<a href="{{ route('about.page') }}">About Us</a>
							</li>
							<li>
								<a href="{{ route('contact.index') }}">Contact Us</a>
							</li>
							<li>
								<a href="{{ route('help.page') }}">Help</a>
							</li>
							<li>
								<a href="{{ route('faqs.page') }}">Faqs</a>
							</li>
							<li>
								<a href="{{ route('termsofuse.page') }}">Terms of use</a>
							</li>
							<li>
								<a href="{{ route('privacy.page') }}">Privacy Policy</a>
							</li>
						</ul>
					</div>
					<div class="col-xs-6 footer-grids">
						<h3>Get in Touch</h3>
						<ul>
							<li>
								<i class="fa fa-map-marker"></i> {{ $companyInfo->address }}</li>
							<li>
								<i class="fa fa-mobile"></i> {{ $companyInfo->phone }} </li>
							<li>
								<i class="fa fa-phone"></i> {{ $companyInfo->fax }} </li>
							<li>
								<i class="fa fa-envelope-o"></i>
								<a href="mailto:example@mail.com"> {{ $companyInfo->email }}</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- //quick links -->
				<!-- social icons -->
				<div class="col-sm-2 footer-grids  w3l-socialmk">
					<h3>Follow Us on</h3>
					<div class="social">
						<ul>
							<li>
								<a class="icon fb" href="{{ $companyInfo->facebook }}" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a class="icon tw" href="{{ $companyInfo->twitter }}" target="_blank">
									<i class="fa fa-twitter"></i>
								</a>
							</li>
							<li>
								<a class="icon gp" href="{{ $companyInfo->gooleplus }}" target="_blank">
									<i class="fa fa-google-plus"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="agileits_app-devices">
						<h5>Download the App</h5>
						<a href="#">
							<img src="{{ asset('frontend')}}/images/1.png" alt="">
						</a>
						<a href="#">
							<img src="{{ asset('frontend')}}/images/2.png" alt="">
						</a>
						<div class="clearfix"> </div>
					</div>
				</div>
				<!-- //social icons -->
				<div class="clearfix"></div>
			</div>
			<!-- //footer third section -->
			<!-- footer fourth section (text) -->
			<div class="agile-sometext">
				<!-- brands -->
				<div class="sub-some">
					<h5>Popular Brands</h5>
					<ul>
						@foreach($brands as $brand)
						<li>
							<a href="{{ route('product.brandwise.data',$brand->slug) }}">{{ $brand->name }}</a>
						</li>
						@endforeach
					</ul>
				</div>
				<!-- //brands -->
				<!-- payment -->
				<div class="sub-some child-momu">
					<h5>Payment Method</h5>
					<ul>
						<li>
							<img src="{{ asset('frontend')}}/images/pay2.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay5.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay1.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay4.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay6.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay3.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay7.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay8.png" alt="">
						</li>
						<li>
							<img src="{{ asset('frontend')}}/images/pay9.png" alt="">
						</li>
					</ul>
				</div>
				<!-- //payment -->
			</div>
			<!-- //footer fourth section (text) -->
		</div>
	</footer>
	<!-- //footer -->
	<!-- copyright -->
	<div class="copy-right">
		<div class="container">
			<p>© {{ date('Y') }} Smart Bazer. All rights reserved | Created by
				<a href=""> Nasir Ahammed.</a>
			</p>
		</div>
	</div>
	<!-- //copyright -->
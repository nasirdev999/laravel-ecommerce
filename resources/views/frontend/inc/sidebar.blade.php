	<div class="side-bar col-md-3">
		<div class="search-hotel">
			<h3 class="agileits-sear-head">Search Here..</h3>
			<form action="#" method="post">
				<input type="search" placeholder="Product name..." name="search" required="">
				<input type="submit" value=" ">
			</form>
		</div>
		<!-- category -->
		<div class="left-side">
			<h3 class="agileits-sear-head">Category</h3>
			<ul>
				@foreach($categoris as $category)
				<li>
					<a href="{{ route('product.categorywise.data',$category->slug) }}">
						<input type="checkbox" class="checked" disabled="">
						<span class="span">{{ $category->name }}</span>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
		<!-- //category -->

		<!-- brand -->
		<div class="left-side">
			<h3 class="agileits-sear-head">Brand</h3>
			<ul>
				@foreach($brands as $brand)
				<li>
					<a href="{{ route('product.brandwise.data',$brand->slug) }}">
						<input type="checkbox" class="checked">
						<span class="span">{{ $brand->name }}</span>
					</a>	
				</li>
				@endforeach
			</ul>
		</div>
		<!-- //brand -->

		
		<!-- cuisine -->
		<div class="left-side">
			<h3 class="agileits-sear-head">Cuisine</h3>
			<ul>
				@foreach($madeCountry as $country)
				<li>
					<a href="{{ route('product.countrywise.data',$country->slug) }}">
						<input type="checkbox" class="checked">
						<span class="span">{{ $country->name }}</span>
					</a>
				</li>
				@endforeach
			</ul>
		</div>
		<!-- //cuisine -->
		<!-- deals -->
		<div class="deal-leftmk left-side">
			<h3 class="agileits-sear-head">Special Deals</h3>
			@foreach($specialProduct as $product)
			<div class="special-sec1">
				<div class="col-xs-4 img-deals">
					<img src="{{ asset('upload/productImage/'.$product->image)}}" alt="" height="60px" width="60px">
				</div>
				<div class="col-xs-8 img-deal1">
					<h3>{{ $product->name }}</h3>
					<a href="{{ route('product.details',$product->slug) }}">$ {{ $product->discount_price }}</a>
				</div>
				<div class="clearfix"></div>
			</div>
			@endforeach

		</div>
		<!-- //deals -->
	</div>
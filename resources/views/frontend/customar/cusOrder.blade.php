@extends('frontend.master')

@section('title')
	customar order details
@endsection
@section('mainsection')

	<div class="ads-grid">
		<div class="container">
			<div class="row">
				<div class="col-md-3 customar_left">
					<ul>
						<li>
							<a href="{{ route('customar.cusdashboard') }}">Dashboard</a>
						</li>
						<li>
							<a href="{{ route('customar.profile') }}">Profile Update</a>
						</li>
						<li>
							<a href="{{ route('customar.password') }}">Password Change</a>
						</li>
						<li>
							<a href="{{ route('customar.order') }}" id="cusActive">Order Details</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
				<div class="customar_right">
					<div class="heading text-center">
						<h2>Customar Orders</h2>
					</div>
					<table class="table table-bordered">
						<tr>
							<th>SL No</th>
							<th>Order Date</th>
							<th>Order No</th>
							<th>Amount</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
						@foreach($order_cus as $key=>$order)
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>{{ $order->order_date }}</td>
							<td>#OR-{{ $order->order_no }}</td>
							<td>{{ $order->order_total }}</td>
							<td>
								@if($order->status == true)
								<span class="label label-success">Success</span>
								@else
								<span class="label label-danger">Pending</span>
								@endif
							</td>
							<td>
								<a class="btn btn-sm btn-info" href="{{ route('customar.order.view',$order->id) }}"><i class="fa fa-eye"></i></a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>


@endsection
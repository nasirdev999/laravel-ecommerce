@extends('frontend.master')

@section('title')
	customar verify account 
@endsection
@section('mainsection')
	<!-- banner-2 -->
	<div class="page-head_agile_info_w3l">

	</div>
	<!-- //banner-2 -->
	<!-- page -->
	<div class="services-breadcrumb">
		<div class="agile_inner_breadcrumb">
			<div class="container">
				<ul class="w3_short">
					<li>
						<a href="{{ route('home') }}">Home</a>
						<i>|</i>
					</li>
					<li>Verify Account</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->
	<!-- FAQ-help-page -->
	<div class="faqs-w3l">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-w3l">Verify Account
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<!-- //tittle heading -->
			<h3 class="alert alert-info">Please Check Your Gmail and Give The Code. <b>Your Account Verify in 24 hours, Outherwise Destroy You Account.</b></h3>
			<div class="faq-w3agile">
				<div class="col-md-6 col-md-offset-3">
				<form action="{{ route('customar.verify.completes') }}" method="post">
					@csrf
					<div class="form-group">
						<label for="">Email Address</label>
						<input type="text" name="email" class="form-control" placeholder="Email Address">
						<font color="red">{{ ($errors->has('email')) ? ($errors->first('email')):''}}</font>
					</div>
					<div class="form-group">
						<label for="">Code</label>
						<input type="text" name="code" class="form-control">
						<font color="red">{{ ($errors->has('code')) ? ($errors->first('code')):''}}</font>
					</div>
					<input type="submit" class="btn btn-primary">
				</form>
				</div>
			</div>
		</div>
	</div>
	<!-- //FAQ-help-page -->

@endsection
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{ asset('frontend') }}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />

	<!-- jquery -->
	<script src="{{ asset('frontend') }}/js/jquery-2.1.4.min.js"></script>
	<!-- for bootstrap working -->
	<script src="{{ asset('frontend') }}/js/bootstrap.js"></script>
	<title>order list pdf</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="customar_right">
				<div class="heading text-center">
					<h2>
					Customar Orders</h2>
					<b>Date : {{ date('d-m-Y',strtotime($orderpdf->order_date)) }}</b><br>
					<b>Order No : #OR-{{ $orderpdf->order_no }}</b>
				</div>
				<div class="invoice">
					<div class="" style="float: left; width: 60%">
	                  <address>
	                    <strong>Shipping Information</strong><br>
	                    {{ $orderpdf->shipping->name }}<br>
	                    {{ $orderpdf->shipping->phone }}<br>
	                    {{ $orderpdf->shipping->address }}<br>
	                    {{ $orderpdf->shipping->city }}<br>
	                  </address>
	                </div>
	                <div class="" style="width: 40%">
	                  <address>
	                    <strong>Payment Information</strong><br>
	                   @if($orderpdf->payment->payment_method == 'cash_payment')
	                   <b>Payment Type : </b> {{ $orderpdf->payment->payment_method }}<br>
	                    @else
	                    <b>Payment Type : </b> {{ $orderpdf->payment->payment_method }}<br>
	                    <b>Mobile Number : </b> {{ $orderpdf->payment->phone }}<br>
	                    <b>Transaction No : </b> {{ $orderpdf->payment->transaction_no }}<br>
	                    @endif
	                  </address>
	                </div>
                </div>

                <table class="table table-bordered">
                	<tr>
                		<th>Product Name</th>
                		<th>Qty</th>
                		<th>Price</th>
                		<th>Amount</th>
                	</tr>
                	@php
                		$sum = 0;
                	@endphp
                	@foreach($orderpdf->order_details as $order)
	                	<tr>
                			<td>{{ $order->product->name }}</td>
                			<td>{{ $order->quantity }}</td>
                			<td>{{ $order->product->discount_price }}</td>
                			<td>{{ $order->quantity * $order->product->discount_price }}</td>
	                	</tr>
	                	@php
	                		$sum += $order->quantity * $order->product->discount_price;
	                	@endphp
                	@endforeach
                </table>
                <table class="table table-bordered" style="margin-left: 400px;">
                	<tr>
                		<th>Subtotal :</th>
                		<td>{{ $sum }}</td>
                	</tr>
                	<tr>
                		<th>Tax :<b>(7%)</b></th>
                		<td>{{ $tax = ($sum/100)*7}}</td>
                	</tr>
                	<tr>
                		<th>Grand Total </th>
                		<td>{{ $grandTotal = $sum + $tax }}</td>
                	</tr>
                </table>
			</div>
			<p>
				............... <br>
				customar
			</p>
		</div>
	</div>
</body>
</html>
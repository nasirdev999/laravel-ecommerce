@extends('frontend.master')

@section('title')
	customar dashboard
@endsection
@section('mainsection')

	<div class="ads-grid">
		<div class="container">
			<div class="row">
				<div class="col-md-3 customar_left">
					<ul>
						<li>
							<a href="{{ route('customar.cusdashboard') }}" id="cusActive">Dashboard</a>
						</li>
						<li>
							<a href="{{ route('customar.profile') }}">Profile Update</a>
						</li>
						<li>
							<a href="{{ route('customar.password') }}">Password Change</a>
						</li>
						<li>
							<a href="{{ route('customar.order') }}">Order Details</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
				<div class="customar_right">
					<div class="heading text-center">
						<img src="{{ asset('upload/customarImage/'.$customar->image) }}" alt="no image" height="70px" width="70px">
						<h3>{{ $customar->name }}</h3>
					</div>
					<table class="table table-bordered">
						<tr>
							<th>Email Address </th>
							<td>{{ $customar->email }}</td>
						</tr>
						<tr>
							<th>Mobile  </th>
							<td>{{ $customar->phone }}</td>
						</tr>
						<tr>
							<th>City </th>
							<td>{{ $customar->city }}</td>
						</tr>
						<tr>
							<th>Address </th>
							<td>{{ $customar->address }}</td>
						</tr>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>

@endsection
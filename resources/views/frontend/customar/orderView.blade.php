@extends('frontend.master')

@section('title')
	customar order details
@endsection
@section('mainsection')

	<div class="ads-grid">
		<div class="container">
			<div class="row">
				<div class="col-md-3 customar_left">
					<ul>
						<li>
							<a href="{{ route('customar.cusdashboard') }}">Dashboard</a>
						</li>
						<li>
							<a href="{{ route('customar.profile') }}">Profile Update</a>
						</li>
						<li>
							<a href="{{ route('customar.password') }}">Password Change</a>
						</li>
						<li>
							<a href="{{ route('customar.order') }}" id="cusActive">Order Details</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
					<div class="customar_right">
						<div class="heading text-center">
							<h2>
							Customar Orders</h2>
							<b>Date : {{ date('d-m-Y',strtotime($orderView->order_date)) }}</b><br>
							<b>Order No : #OR-{{ $orderView->order_no }}</b>
						</div>
						<div class="invoice">
							<div class="pull-left">
			                  <address>
			                    <strong>Shipping Information</strong><br>
			                    {{ $orderView->shipping->name }}<br>
			                    {{ $orderView->shipping->phone }}<br>
			                    {{ $orderView->shipping->address }}<br>
			                    {{ $orderView->shipping->city }}<br>
			                  </address>
			                </div>
			                <div class="pull-right">
			                  <address>
			                    <strong>Payment Information</strong><br>
			                   @if($orderView->payment->payment_method == 'cash_payment')
			                   <b>Payment Type : </b> {{ $orderView->payment->payment_method }}<br>
			                    @else
			                    <b>Payment Type : </b> {{ $orderView->payment->payment_method }}<br>
			                    <b>Mobile Number : </b> {{ $orderView->payment->phone }}<br>
			                    <b>Transaction No : </b> {{ $orderView->payment->transaction_no }}<br>
			                    @endif
			                  </address>
			                </div>
		                </div>

		                <table class="table table-bordered">
		                	<tr>
		                		<th>Product Name</th>
		                		<th>Qty</th>
		                		<th>Price</th>
		                		<th>Amount</th>
		                	</tr>
		                	@php
		                		$sum = 0;
		                	@endphp
		                	@foreach($orderView->order_details as $order)
			                	<tr>
		                			<td>{{ $order->product->name }}</td>
		                			<td>{{ $order->quantity }}</td>
		                			<td>{{ $order->product->discount_price }}</td>
		                			<td>{{ $order->quantity * $order->product->discount_price }}</td>
			                	</tr>
			                	@php
			                		$sum += $order->quantity * $order->product->discount_price;
			                	@endphp
		                	@endforeach
		                </table>
		                <table class="table table-bordered" style="width:50%; float: right;">
		                	<tr>
		                		<th>Subtotal :</th>
		                		<td>{{ $sum }}</td>
		                	</tr>
		                	<tr>
		                		<th>Tax :<b>(7%)</b></th>
		                		<td>{{ $tax = ($sum/100)*7}}</td>
		                	</tr>
		                	<tr>
		                		<th>Grand Total </th>
		                		<td>{{ $grandTotal = $sum + $tax }}</td>
		                	</tr>
		                </table>
		                <div class="form-group">
		                	<a target="__blank" href="{{ route('customar.order.list.pdf',$orderView->id) }}" class="btn btn-info">pdf generate</a>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
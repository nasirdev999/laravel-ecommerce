@extends('frontend.master')

@section('title')
	customar edit
@endsection
@section('mainsection')

	<div class="ads-grid">
		<div class="container">
			<div class="row">
				<div class="col-md-3 customar_left">
					<ul>
						<li>
							<a href="{{ route('customar.cusdashboard') }}">Dashboard</a>
						</li>
						<li>
							<a href="{{ route('customar.profile') }}" id="cusActive">Profile Update</a>
						</li>
						<li>
							<a href="{{ route('customar.password') }}">Password Change</a>
						</li>
						<li>
							<a href="{{ route('customar.order') }}">Order Details</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
				<div class="customar_right">
					<div class="heading text-center">
						<h3><b>{{ $customarEdit->name }}</b> Your Profile Update</h3><br>
					</div>
					<form action="{{ route('customar.profile.update',$customarEdit->id) }}" method="post" enctype="multipart/form-data">
						@csrf
						<table class="table table-bordered">
							<tr>
								<th>Image </th>
								<td>
									<input type="file" class="form-control" name="image">
									<img src="{{ asset('upload/customarImage/'.$customarEdit->image) }}" alt="" height="50px" width="50px">
								</td>
							</tr>
							<tr>
								<th>Name </th>
								<td><input type="text" value="{{ $customarEdit->name }}" class="form-control" name="name"></td>
							</tr>
							<tr>
								<th>Email Address </th>
								<td><input type="text" value="{{ $customarEdit->email }}" class="form-control" name="email"></td>
							</tr>
							<tr>
								<th>Mobile  </th>
								<td><input type="text" value="{{ $customarEdit->phone }}" class="form-control" name="phone"></td>
							</tr>
							<tr>
								<th>City </th>
								<td><input type="text" value="{{ $customarEdit->city }}" class="form-control" name="city"></td>
							</tr>
							<tr>
								<th>Address </th>
								<td><input type="text" value="{{ $customarEdit->address }}" class="form-control" name="address"></td>
							</tr>
						</table>
						<input type="submit" class="btn btn-primary">
					</form>
				</div>
				</div>
			</div>
		</div>
	</div>

@endsection
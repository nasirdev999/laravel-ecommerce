@extends('frontend.master')

@section('title')
	customar passwordd change
@endsection
@section('mainsection')

	<div class="ads-grid">
		<div class="container">
			<div class="row">
				<div class="col-md-3 customar_left">
					<ul>
						<li>
							<a href="{{ route('customar.cusdashboard') }}">Dashboard</a>
						</li>
						<li>
							<a href="{{ route('customar.profile') }}">Profile Update</a>
						</li>
						<li>
							<a href="{{ route('customar.password') }}" id="cusActive">Password Change</a>
						</li>
						<li>
							<a href="{{ route('customar.order') }}">Order Details</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
				<div class="customar_right">
					<div class="heading text-center">
						<h3><b>{{ $customarPassword->name }}</b> Your Password Change</h3><br>
					</div>
					<form action="{{ route('customar.password.update',$customarPassword->id) }}" method="post">
						@csrf
						<div class="form-group">
							<label for="">Old Password</label>
							<input type="password" name="old_password" class="form-control">
						</div>
						<div class="form-group">
							<label for="">New Password</label>
							<input type="password" name="password" class="form-control">
						</div>
						<div class="form-group">
							<label for="">Confirm Password</label>
							<input type="password" name="password_confirmation" class="form-control">
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary">
						</div>
					</form>
				</div>
				</div>
			</div>
		</div>
	</div>

@endsection